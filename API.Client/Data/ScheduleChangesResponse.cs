﻿using RoosterOil.Shared;

namespace RoosterOil.DisplayAPI.Client.Data
{
    public class ScheduleChangesResponse : WorkloadResponse
    {
        public Schedule newSchedule { get; set; }
    }
}
