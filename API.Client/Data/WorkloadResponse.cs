﻿using System;

namespace RoosterOil.DisplayAPI.Client.Data
{
    public abstract class WorkloadResponse
    {
        public DateTime responseTime { get; set; }
        public int sequence { get; set; }
    }
}
