﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using RestSharp;
using RoosterOil.DisplayAPI.Client.Data;
using RoosterOil.Shared;

namespace RoosterOil.DisplayAPI.Client
{
    public class DeviceClient
    {
        private string _apiURL;

        private DeviceClient()
        {

        }

        public DeviceClient(string apiURL)
        {
            _apiURL = apiURL;
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public async Task<ScheduleChangesResponse> GetScheduleChanges(string deviceId, string scheduleId, DateTime lastUpdated)
        {
            var client = new RestClient(_apiURL);
            var request = new RestRequest($"api/display/getschedule/{deviceId}/{scheduleId}/{lastUpdated.ToUniversalTime().ToString("o")}", Method.GET);

            var response = await client.ExecuteAsync<ScheduleChangesResponse>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }
            return response.Data;
        }


        public Task<bool> GetCreative(Creative creative, string destinationFolder)
        {
            var client = new RestClient(_apiURL);
            var destinationPath = Path.Combine(destinationFolder, creative.CreativeFileName);
            using (var writer = File.OpenWrite(destinationPath))
            {
                var request = new RestRequest("api/display/cacheitem/" + creative._id);
                request.ResponseWriter = responseStream =>
                {
                    using (responseStream)
                    {
                        responseStream.CopyTo(writer);
                    }
                };

                var response = client.DownloadData(request);
                writer.Close();
            }
            return Task.FromResult(File.Exists(destinationPath));
        }

        public Task<bool> DownloadFile(string fileName, string destinationFolder)
        {
            var client = new RestClient(_apiURL);

            using (var writer = File.OpenWrite(destinationFolder + fileName))
            {

                var request = new RestRequest("api/display/download/" + fileName);
                request.ResponseWriter = responseStream =>
                {
                    using (responseStream)
                    {
                        responseStream.CopyTo(writer);
                    }
                };
                var response = client.DownloadData(request);
                writer.Close();
            }
            return Task.FromResult(true);
        }
    }
}
