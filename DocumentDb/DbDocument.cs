﻿using MySqlX.XDevAPI;
using MySqlX.XDevAPI.Common;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RoosterOil.Utility;

namespace RoosterOil.DocumentDb
{
    public class DocumentDbException : Exception
    {
        public DocumentDbException() : base() { }
        public DocumentDbException(string message) : base(message) { }
        public DocumentDbException(string message, Exception exception) : base(message, exception) { }
    }

    public abstract class DbDocument
    {
        public string _id { get; set; }
        public DateTime _createdTime { get; set; }
        public DateTime _modifiedTime { get; set; }

        public static string CollectionName<T>() => $"{typeof(T).Name}s";

        public DbDocument()
        {
            _id = null;
        }

        protected virtual bool HasRequiredFields() { return true; }
        protected virtual Task<bool> IsUnique(Schema db) { return Task.FromResult(true); }
        protected virtual Task<bool> PostRead() { return Task.FromResult(true); }
        protected virtual Task<bool> PreWrite() { return Task.FromResult(true); }
        protected virtual Task<bool> PostWrite() { return Task.FromResult(true); }

        protected virtual Task<bool> DeleteChildren(Schema db, LookupKey parentKey) { return Task.FromResult(true);  }

        protected static ILogger _logger;

        internal static async Task<IEnumerable<T>> GetAllDocuments<T>(Schema schema, string queryString = null) where T : DbDocument
        {
            List<T> returnCollection = new List<T>();

            foreach (var serializedItem in schema.GetCollection(CollectionName<T>()).Find(queryString).Execute())
            {
                T item = JsonConvert.DeserializeObject<T>(serializedItem.ToString());
                if (!await item.PostRead().ConfigureAwait(false))
                {
                    throw new DocumentDbException("Document read failed during post-read processing.");
                }

                returnCollection.Add(item);
            }

            return returnCollection;
        }

        internal static Task<List<LookupKey>> GetLookupKeys<T>(Schema schema, string queryString = null) where T : DbDocument
        {
            List<LookupKey> returnCollection = new List<LookupKey>();

            try
            {
                foreach (var serializedItem in schema.GetCollection(CollectionName<T>()).Find(queryString).Fields("_id").Execute())
                {
                    LookupKey key = JsonConvert.DeserializeObject<LookupKey>(serializedItem.ToString());

                    returnCollection.Add(key);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Task.FromResult(returnCollection);
        }

        public static Schema GetStore(ILogger logger)
        {
            var options = DocStoreConfiguration.GetDocStoreConfiguration()._docStoreOptions;
            _logger = logger;

            MySqlXConnectionStringBuilder mySqlConnectionStringBuilder = new MySqlXConnectionStringBuilder();
            mySqlConnectionStringBuilder.Port = options.Port;
            mySqlConnectionStringBuilder.Server = options.Server;
            mySqlConnectionStringBuilder.UserID = options.User;
            mySqlConnectionStringBuilder.Password = options.Password;
            mySqlConnectionStringBuilder.CharacterSet = "utf8mb4";
            mySqlConnectionStringBuilder.Compression = MySql.Data.MySqlClient.CompressionType.Required;

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };

            return MySQLX.GetSession(mySqlConnectionStringBuilder.GetConnectionString(true)).GetSchema(options.Schema);
        }

        public static Collection CreateCollection<T>(Schema schema)
        {
            var classCollection = schema.GetCollection(CollectionName<T>());
            if (!classCollection.ExistsInDatabase())
            {
                classCollection = schema.CreateCollection(CollectionName<T>());
            }

            return classCollection;
        }

        public static Collection FindCollection<T>(Schema schema)
        {
            var classCollection = schema.GetCollection(CollectionName<T>());
            if (!classCollection.ExistsInDatabase())
            {
                throw new DocumentDbException($"Collection {CollectionName<T>()} does not exist.");
            }

            return classCollection;
        }

        public static async Task<Result> Create<T>(Schema schema, T item) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            Result returnResult = null;

            if (!item.IsNew())
            {
                throw new DocumentDbException("Attempt to add existing record.");
            }

            if (!item.HasRequiredFields() || !await item.IsUnique(schema))
            {
                throw new DocumentDbException("Data validation failed");
            }

            var classCollection = FindCollection<T>(schema);

            item._modifiedTime = DateTime.Now;
            if (!await item.PreWrite().ConfigureAwait(false))
            {
                throw new DocumentDbException("Document not committed Pre-write handling failed.");
            }

            item._createdTime = DateTime.Now;
            returnResult = classCollection.Add(JsonConvert.SerializeObject(await item.Normalize())).Execute();
            item._id = returnResult.GeneratedIds[0];

            if (!await item.PostWrite().ConfigureAwait(false))
            {
                throw new DocumentDbException("Post-write handling failed after commit.");
            }

            return returnResult;
        }

        public static async Task<Result> Update<T>(Schema schema, T item) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            Result returnResult = null;

            if (item.IsNew())
            {
                throw new DocumentDbException("Cannot update item with no database id (Has the item been added?)");
            }

            var classCollection = FindCollection<T>(schema);

            if (!item.HasRequiredFields() || !await item.IsUnique(schema))
            {
                throw new DocumentDbException("Data validation failed");
            }

            item._modifiedTime = DateTime.Now;
            if (!await item.PreWrite().ConfigureAwait(false))
            {
                throw new DocumentDbException("Document not committed Pre-write handling failed.");
            }

            returnResult = classCollection.ReplaceOne(item._id, JsonConvert.SerializeObject(await item.Normalize()));

            if (!await item.PostWrite().ConfigureAwait(false))
            {
                throw new DocumentDbException("Post-write handling failed after commit.");
            }

            return returnResult;
        }

        public static async Task<Result> Delete<T>(Schema schema, T item) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            if (item.IsNew())
            {
                throw new DocumentDbException("Cannot delete item with no database id (Has the item been added?)");
            }

            var classCollection = FindCollection<T>(schema);

            if (!await item.DeleteChildren(schema, new LookupKey(item._id)).ConfigureAwait(false))
            {
                throw new DocumentDbException("Failed deleting children of item.");
            }

            return classCollection.RemoveOne(item._id);
        }

        public static async Task<T> FetchByKey<T>(Schema schema, LookupKey lookupKey) where T : DbDocument
        {
            var serializedItem = schema.GetCollection(CollectionName<T>()).Find("_id = :id").Bind("id", lookupKey._id).Execute().FirstOrDefault();
            
            if (serializedItem == null)
            {
                return null;
            }

            T item = JsonConvert.DeserializeObject<T>(serializedItem.ToString());
            if (!await item.PostRead().ConfigureAwait(false))
            {
                throw new DocumentDbException("Document read failed during post-read processing.");
            }

            return item;
        }

        public static async Task<List<T>> FetchByKeys<T>(Schema schema, List<LookupKey> lookupKeys) where T : DbDocument
        {
            var resultList = new List<T>();

            foreach (var serializedItem in schema.GetCollection(CollectionName<T>()).Find(Query.In<T>(nameof(DbDocument._id), lookupKeys)).Execute())
            {
                T item = JsonConvert.DeserializeObject<T>(serializedItem.ToString());
                if (!await item.PostRead().ConfigureAwait(false))
                {
                    throw new DocumentDbException("Document read failed during post-read processing.");
                }

                resultList.Add(item);
            }

            return resultList;
        }

        public bool IsNew()
        {
            return String.IsNullOrEmpty(this._id);
        }
    }
}
