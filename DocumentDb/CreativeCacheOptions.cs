﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.DocumentDb
{
    public class CreativeCacheOptions
    {
        public const string CacheInformation = "LocalCache";
        public string Path { get; set; }
        public int RefreshRate { get; set; }
    }
}
