﻿using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;
using System;

namespace RoosterOil.DocumentDb
{
    public class DocStoreConfiguration
    {
        public DocumentDbOptions _docStoreOptions { get; set; }
        public RemoteSystemOptions _remoteSystemOptions { get; set; }
        public VideoEncoderOptions _videoEncoderOptions { get; set; }

        public CreativeCacheOptions _creativeCacheOptions { get; set; }

        private static DocStoreConfiguration globalOptions = null;

        protected static bool InvalidString(ILogger _logger, string className, string propertyName, string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                _logger.LogError($"Settings: \"{className}\": \"{propertyName}\" must have a value.");
                return true;
            }

            return false;
        }
        protected static bool IsValid(ILogger _logger, DocumentDbOptions documentDbOptions)
        {
            if (InvalidString(_logger, nameof(DocumentDbOptions), nameof(documentDbOptions.Server), documentDbOptions.Server) ||
                InvalidString(_logger, nameof(DocumentDbOptions), nameof(documentDbOptions.Schema), documentDbOptions.Schema) ||
                InvalidString(_logger, nameof(DocumentDbOptions), nameof(documentDbOptions.User), documentDbOptions.User) ||
                InvalidString(_logger, nameof(DocumentDbOptions), nameof(documentDbOptions.Schema), documentDbOptions.Schema))
            {
                return false;
            }

            return true;
        }

        protected static bool IsValid(ILogger _logger, RemoteSystemOptions remoteSystemOptions)
        {
            if (InvalidString(_logger, nameof(RemoteSystemOptions), nameof(remoteSystemOptions.System), remoteSystemOptions.System) ||
                InvalidString(_logger, nameof(RemoteSystemOptions), nameof(remoteSystemOptions.User), remoteSystemOptions.User) ||
                InvalidString(_logger, nameof(RemoteSystemOptions), nameof(remoteSystemOptions.KeyFile), remoteSystemOptions.KeyFile) ||
                InvalidString(_logger, nameof(RemoteSystemOptions), nameof(remoteSystemOptions.SSHCommand), remoteSystemOptions.SSHCommand) ||
                InvalidString(_logger, nameof(RemoteSystemOptions), nameof(remoteSystemOptions.SCPCommand), remoteSystemOptions.SCPCommand))
            {
                return false;
            }

            return true;
        }

        protected static bool IsValid(ILogger _logger, VideoEncoderOptions videoEncoderOptions)
        {
            if (InvalidString(_logger, nameof(VideoEncoderOptions), nameof(videoEncoderOptions.EncoderName), videoEncoderOptions.EncoderName) ||
                InvalidString(_logger, nameof(VideoEncoderOptions), nameof(videoEncoderOptions.EncoderPath), videoEncoderOptions.EncoderPath) ||
                InvalidString(_logger, nameof(VideoEncoderOptions), nameof(videoEncoderOptions.EncodeExtension), videoEncoderOptions.EncodeExtension) ||
                InvalidString(_logger, nameof(VideoEncoderOptions), nameof(videoEncoderOptions.ProbeName), videoEncoderOptions.ProbeName))
            {
                return false;
            }

            return true;
        }

        protected static bool IsValid(ILogger _logger, CreativeCacheOptions creativeCacheOptions)
        {
            if (InvalidString(_logger, nameof(CreativeCacheOptions), nameof(creativeCacheOptions.Path), creativeCacheOptions.Path))
            {
                return false;
            }
            if (creativeCacheOptions.RefreshRate < 1)
            {
                _logger.LogError($"Settings: \"CreativeCacheOptions\": \"RefreshRate\" must be greater than 1 second.");
                return false;
            }

            return true;
        }
        public static void SetDocStoreConfiguration(ILogger _logger, DocumentDbOptions docStoreOptions, RemoteSystemOptions remoteSystemOptions, VideoEncoderOptions videoEncoderOptions, CreativeCacheOptions cacheInfo)
        {
            if (globalOptions == null)
            {
                _logger.LogInformation("Configuring...");
                if (!IsValid(_logger, docStoreOptions) || !IsValid(_logger, remoteSystemOptions) || !IsValid(_logger, videoEncoderOptions) || !IsValid(_logger, cacheInfo))
                {
                    throw new Exception("BAD CONFIGURATION!");
                }

                globalOptions = new DocStoreConfiguration()
                {
                    _docStoreOptions = docStoreOptions,
                    _remoteSystemOptions = remoteSystemOptions,
                    _videoEncoderOptions = videoEncoderOptions,
                    _creativeCacheOptions = cacheInfo
                };
                _logger.LogInformation("..Done");
            }
        }

        public static DocStoreConfiguration GetDocStoreConfiguration()
        {
            if (globalOptions == null)
            {
                throw new DocumentDbException("Document store not initialized.");
            }

            return globalOptions;
        }
    }
}
