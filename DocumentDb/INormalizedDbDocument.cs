﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.DocumentDb
{
    public interface INormalizedDbDocument<T>
    {
        public Task<T> Normalize();
    }
}
