﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoosterOil.Utility;
using System.Threading;

namespace RoosterOil.DocumentDb
{
    public class InitializationService : BackgroundService
    {
        public InitializationService(ILogger<InitializationService> _logger, IOptions<DocumentDbOptions> dbOptions, IOptions<RemoteSystemOptions> remoteOptions, IOptions<VideoEncoderOptions> videoOptions,
            IOptions<CreativeCacheOptions> cacheInfo)
        {
            DocStoreConfiguration.SetDocStoreConfiguration(_logger, dbOptions.Value, remoteOptions.Value, videoOptions.Value, cacheInfo.Value);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }
    }
}
