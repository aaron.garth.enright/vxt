﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.DocumentDb
{
    public class DocumentDbOptions
    {
        public const string DocumentDb = "DocumentDb";
        public string Server { get; set; }
        public string Schema { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public uint Port { get; set; }
        public bool ReEncodeVideos { get; set; }
    }
}
