﻿namespace RoosterOil.DocumentDb
{
    public static class LookupKeyStringExtensions
    {
        public static LookupKey ToLookupKey(this string key)
        {
            return new LookupKey(key);
        }
    }

    public class LookupKey
    {
        public LookupKey(string id)
        {
            _id = id;
        }

        public string _id { get; set; }

        public override string ToString()
        {
            return _id;
        }
    }
}
