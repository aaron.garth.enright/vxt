﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.DocumentDb
{
    public static class Query
    {
        internal static string Bind(object value)
        {
            var valueType = value.GetType().Name;

            switch (valueType)
            {
                case "String":
                case "Char":
                    return $"\"{value}\"";
                case "DateTime":
                    return $"\"{((DateTime)value).ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")}\"";
                case "Boolean":
                    return $"{((bool)value).ToString().ToLower()}";
            }

            return value.ToString();
        }

        internal static bool CheckField<T>(string field) where T : DbDocument
        {
            if (typeof(T).GetProperty(field) == null)
            {
                throw new DocumentDbException($"Invalid property {field} on type {typeof(T).Name}.");
            }
            return true;
        }

        internal static string QueryToString(string field, string value, string op)
        {
            return $"{field} {op} {value}";
        }

        public static string EQ<T>(string field, object value) where T : DbDocument
        {
            CheckField<T>(field);

            return QueryToString(field, Bind(value), "=");
        }

        public static string NEQ<T>(string field, object value) where T : DbDocument
        {
            CheckField<T>(field);
            return QueryToString(field, Bind(value), "!=");
        }

        public static string LIKE<T>(string field, string value) where T : DbDocument
        {
            CheckField<T>(field);

            return QueryToString(field, $"'%{value}%'", "like");
        }

        internal static string In<T>(string field, List<object> values) where T : DbDocument
        {
            Query.CheckField<T>(field);

            string valueList = "";

            foreach (var value in values)
            {
                valueList += Bind(value) + ',';
            }

            valueList = valueList.Substring(0, valueList.Length - 1);

            return $"{field} in [ {valueList} ]";
        }

        internal static string In<T>(string field, List<LookupKey> keys) where T : DbDocument
        {
            Query.CheckField<T>(field);

            string valueList = "";

            foreach (var value in keys)
            {
                valueList += Bind(value._id) + ',';
            }

            valueList = valueList.Substring(0, valueList.Length - 1);

            return $"{field} in [ {valueList} ]";
        }

        internal static string In<T>(string field, string valueString, char separator = ',') where T : DbDocument
        {
            Query.CheckField<T>(field);

            string[] values = valueString.Split(separator);

            string valueList = "";

            foreach (var value in values)
            {
                valueList += Bind(value) + ',';
            }

            valueList = valueList.Substring(0, valueList.Length - 1);

            return $"{field} in [ {valueList} ]";
        }

        public static string AND(string field, string value)
        {
            return QueryToString(field, value, "and");
        }

        public static string OR(string field, string value)
        {
            return QueryToString(field, value, "or");
        }
    }
}
