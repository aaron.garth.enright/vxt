﻿using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoosterOil.Utility;

namespace RoosterOil.DocumentDb
{
    public static class DocumentDbQuery
    {
        public static async Task<bool> Exists<T>(Schema db, string field, object value) where T : DbDocument
        {
            return (await DbDocument.GetLookupKeys<T>(db, Query.EQ<T>(field, value)).ConfigureAwait(false)).Count > 0;
        }

        public static async Task<bool> OtherExists<T>(Schema db, LookupKey me, string field, object value) where T : DbDocument
        {
            return (await DbDocument.GetLookupKeys<T>(db, Query.AND(Query.NEQ<T>(nameof(DbDocument._id), me), Query.EQ<T>(field, value))).ConfigureAwait(false)).Count > 0;
        }

        public static async Task<T> Get<T>(Schema db, string id) where T : DbDocument
        {
            return await DbDocument.FetchByKey<T>(db, id.ToLookupKey()).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetAllItems<T>(Schema db) where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetItemsMatching<T>(Schema db, string query) where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db, query).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetItemsMatching<T>(Schema db, string field, object value) where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db, Query.EQ<T>(field, value)).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetItemsContaining<T>(Schema db, string field, string value) where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db, Query.LIKE<T>(field, value)).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetItemsIn<T>(Schema db, string field, string values, char separator = ',') where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db, Query.In<T>(field, values, separator)).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetItemsIn<T>(Schema db, string field, List<object> values) where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db, Query.In<T>(field, values)).ConfigureAwait(false);
        }

        public static async Task<IEnumerable<T>> GetItemsIn<T>(Schema db, string field, List<LookupKey> values) where T : DbDocument
        {
            return await DbDocument.GetAllDocuments<T>(db, Query.In<T>(field, values)).ConfigureAwait(false);
        }

        public static async Task<List<LookupKey>> GetKeysMatching<T>(Schema db, string query) where T : DbDocument
        {
            return await DbDocument.GetLookupKeys<T>(db, query).ConfigureAwait(false);
        }

        public static async Task<List<LookupKey>> GetKeysMatching<T>(Schema db, string field, object value) where T : DbDocument
        {
            return await DbDocument.GetLookupKeys<T>(db, Query.EQ<T>(field, value)).ConfigureAwait(false);
        }

        public static async Task<List<LookupKey>> GetKeysContaining<T>(Schema db, string field, string value) where T : DbDocument
        {
            return await DbDocument.GetLookupKeys<T>(db, Query.LIKE<T>(field, value)).ConfigureAwait(false);
        }

        public static async Task<List<LookupKey>> GetKeysIn<T>(Schema db, string field, string values, char separator = ',') where T : DbDocument
        {
            return await DbDocument.GetLookupKeys<T>(db, Query.In<T>(field, values, separator)).ConfigureAwait(false);
        }

        public static async Task<List<LookupKey>> GetKeysIn<T>(Schema db, string field, List<object> values) where T : DbDocument
        {
            return await DbDocument.GetLookupKeys<T>(db, Query.In<T>(field, values)).ConfigureAwait(false);
        }

        public static async Task<List<LookupKey>> GetKeysIn<T>(Schema db, string field, List<LookupKey> values) where T : DbDocument
        {
            return await DbDocument.GetLookupKeys<T>(db, Query.In<T>(field, values)).ConfigureAwait(false);
        }

        public static async Task DeleteItemsMatching<T>(Schema db, string field, object value) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            foreach (T item in await DocumentDbQuery.GetItemsMatching<T>(db, field, value).ConfigureAwait(false))
            {
                await DbDocument.Delete<T>(db, item);
            }
        }
    }
}
