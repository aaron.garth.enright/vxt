﻿using System;

namespace RoosterOil.DeviceAPI.Client.Data
{
    public abstract class WorkloadResponse
    {
        public DateTime responseTime { get; set; }
        public int sequence { get; set; }
    }
}
