﻿using RoosterOil.Shared;

namespace RoosterOil.DeviceAPI.Client.Data
{
    public class ScheduleChangesResponse : WorkloadResponse
    {
        public Schedule newSchedule { get; set; }
    }
}
