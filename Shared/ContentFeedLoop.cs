﻿using MySqlX.XDevAPI;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;

namespace RoosterOil.Shared
{
    public class ContentFeedLoop : DbDocument, INormalizedDbDocument<ContentFeedLoop>, IAPIAccessible
    {
        public const string apiLocation = "api/data/contentfeedloop";

        public string _accountId { get; set; }
        public string name { get; set; }
        public List<ContentFeedSlot> contentFeedSlots { get; set; }
        private static int currentSlot { get; set; }

        public ContentFeedLoop()
        {
            currentSlot = 0;
            contentFeedSlots = new List<ContentFeedSlot>();
        }

        protected override bool HasRequiredFields()
        {
            if (this._accountId == "")
            {
                throw new DocumentDbException("Content feed loops must be associated with an account.");
            }

            ConcurrentDictionary<int, bool> usedSlots = new ConcurrentDictionary<int, bool>();

            foreach (var contentFeedSlot in contentFeedSlots)
            {
                if (usedSlots.ContainsKey(contentFeedSlot.loopPosition))
                {
                    throw new DocumentDbException("Duplicate loop position for slot in content feed loop.");
                }
                else
                {
                    usedSlots.TryAdd(contentFeedSlot.loopPosition, true);
                }
                if (contentFeedSlot.secondsToRun <= 0)
                {
                    throw new DocumentDbException("Content feed slot invalid number of seconds to run.");
                }
            }
            return true;
        }

        protected override async Task<bool> IsUnique(Schema db)
        {
            var contentFeedLoops = await DocumentDbQuery.GetItemsMatching<ContentFeedLoop>(db, nameof(ContentFeedLoop._accountId), _accountId).ConfigureAwait(false);

            foreach (var contentFeedLoop in contentFeedLoops)
            {
                if (contentFeedLoop.name == this.name && contentFeedLoop._id != this._id)
                {
                    throw new DocumentDbException("Content feed loops require a unique name.");
                }
            }

            return true;
        }

        public Task<ContentFeedLoop> Normalize()
        {
            var contentFeedLoop = new ContentFeedLoop()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                name = this.name
            };

            foreach (var contentFeedSlot in contentFeedSlots)
            {
                contentFeedLoop.contentFeedSlots.Add(new ContentFeedSlot()
                {
                    _assignedFeedId = (String.IsNullOrWhiteSpace(contentFeedSlot._assignedFeedId) && contentFeedSlot.assignedFeed != null) ? contentFeedSlot.assignedFeed._id : contentFeedSlot._assignedFeedId,
                    assignedFeed = null,
                    isEnabled = contentFeedSlot.isEnabled,
                    loopPosition = contentFeedSlot.loopPosition,
                    secondsToRun = contentFeedSlot.secondsToRun
                });
            }

            return Task.FromResult(contentFeedLoop);
        }

        protected override Task<bool> PreWrite()
        {
            foreach (var contentFeedSlot in contentFeedSlots)
            {
                if (String.IsNullOrEmpty(contentFeedSlot._assignedFeedId) && contentFeedSlot.assignedFeed != null)
                {
                    contentFeedSlot._assignedFeedId = contentFeedSlot.assignedFeed._id;
                }
            }
            return Task.FromResult(true);
        }
    }
}
