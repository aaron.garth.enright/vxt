﻿using MySqlX.XDevAPI;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;

namespace RoosterOil.Shared
{
    public class Creative : DbDocument, INormalizedDbDocument<Creative>, IAPIAccessible
    {
        public const string apiLocation = "api/data/creative";

        public string _accountId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string encoding { get; set; }
        public long size { get; set; }
        public string checkSum { get; set; }
        public string storageExt { get; set; }
        public UnitPair originalResolution { get; set; }
        [JsonIgnore]
        public byte[] fileData { get; set; }

        public Creative()
        {
        }

        private long LoadFileData(string fileName)
        {
            long returnLength = 0L;

            var localFile = File.OpenRead(fileName);

            using (var fileReader = new BinaryReader(localFile))
            {
                returnLength = localFile.Length;
                fileData = new byte[localFile.Length];

                fileData = fileReader.ReadBytes((int)returnLength);
                fileReader.Close();
            }

            return returnLength;
        }

        public static string NameFromFileName(string fileName)
        {
            return Path.GetFileName(fileName);
        }

        public static async Task<Creative> CreateFromFile(string inputFileName, string fileDescription = "")
        {
            var resolution = new Size(1920, 1080);

            if (!File.Exists(inputFileName))
            {
                throw new DocumentDbException($"Creative file not found {inputFileName}");
            }

            var fileToLoad = inputFileName;
            if (FileData.GetContentTypeFromMIMETable(inputFileName).StartsWith("video") || Path.GetExtension(inputFileName).ToLower() == ".flv")
            {
                var videoReEncoder = new VideoEncoder(DocStoreConfiguration.GetDocStoreConfiguration()._videoEncoderOptions, null, _logger);

                resolution = await videoReEncoder.GetSize(inputFileName).ConfigureAwait(false);

                if (DocStoreConfiguration.GetDocStoreConfiguration()._docStoreOptions.ReEncodeVideos && Path.GetExtension(inputFileName) != videoReEncoder.TargetEncoding())
                {
                    Console.WriteLine($"Converting {inputFileName}");
                    if (_logger != null)
                    {
                        _logger.LogInformation($"Converting file {inputFileName}");
                    }

                    fileToLoad = await videoReEncoder.ReEncode(inputFileName, Path.GetTempFileName()).ConfigureAwait(false);
                }
            }
            else if (FileData.GetContentTypeFromMIMETable(inputFileName).StartsWith("image"))
            {
                var image = Image.FromFile(inputFileName);

                resolution = new Size(image.Width, image.Height);
            }

            var newCreative = new Creative()
            {
                encoding = FileData.GetContentTypeFromMIMETable(inputFileName),
                name = NameFromFileName(inputFileName),
                description = fileDescription,
                storageExt = Path.GetExtension(fileToLoad),
                originalResolution = (resolution.Width == 0 && resolution.Height == 0) ? null : UnitPair.FromSize(resolution, "px")
            };
            newCreative.size = newCreative.LoadFileData(fileToLoad);
            newCreative.checkSum = FileData.Checksum(newCreative.fileData);

            if (inputFileName != fileToLoad)
            {
                File.Delete(fileToLoad);
            }

            return newCreative;
        }

        public string CreativeFileName => this._id + this.storageExt;

        public async Task<bool> CacheUpToDate(string cachePath)
        {
            var creativeFileName = Path.Combine(cachePath, CreativeFileName);
            if (!(File.Exists(creativeFileName) && await FileData.CompareChecksum(creativeFileName, this.size, this.checkSum)))
            {
                return false;
            }

            return true;
        }

        protected override bool HasRequiredFields()
        {
            if (String.IsNullOrEmpty(_accountId))
            {
                throw new DocumentDbException("Creatives require an account id.");
            }

            if (String.IsNullOrEmpty(name))
            {
                throw new DocumentDbException("Creatives require a name.");
            }
            return true;
        }

        protected override async Task<bool> IsUnique(Schema db)
        {
            var creatives = await DocumentDbQuery.GetItemsMatching<Creative>(db, nameof(Creative.name), name).ConfigureAwait(false);

            foreach (var creative in creatives)
            {
                if (creative.name == this.name && creative._accountId == this._accountId && creative._id != this._id)
                {
                    throw new DocumentDbException("Creatives require a unique name.");
                }
            }

            return true;
        }

        public Task<Creative> Normalize()
        {
            return Task.FromResult(new Creative()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                _accountId = this._accountId,
                fileData = null,
                encoding = this.encoding,
                name = this.name,
                description = this.description,
                size = this.size,
                checkSum = this.checkSum,
                storageExt = this.storageExt,
                originalResolution = this.originalResolution
            });
        }

        protected override Task<bool> PreWrite()
        {
            new RemoteSystem(DocStoreConfiguration.GetDocStoreConfiguration()._remoteSystemOptions).CreateDirectory(this._accountId);

            if (fileData != null && fileData.Length > 0)
            {
                this.checkSum = FileData.Checksum(this.fileData);
            }

            return Task.FromResult(true);
        }

        protected override Task<bool> PostWrite()
        {
            var remoteSystem = new RemoteSystem(DocStoreConfiguration.GetDocStoreConfiguration()._remoteSystemOptions);
            if (fileData != null)
            {
                string creativeFileName = Path.GetTempPath() + this.CreativeFileName;

                using (var fileWriter = new BinaryWriter(File.OpenWrite(creativeFileName)))
                {
                    fileWriter.Write(this.fileData, 0, (int)this.size);
                    fileWriter.Close();
                }

                remoteSystem.CopyTo(creativeFileName, $"{remoteSystem.HomeDirectory()}{this._accountId}");
                File.Delete(creativeFileName);
            }

            return Task.FromResult(true);
        }

        protected override Task<bool> DeleteChildren(Schema db, LookupKey parentKey)
        {
            new RemoteSystem(DocStoreConfiguration.GetDocStoreConfiguration()._remoteSystemOptions).DestroyDirectory(this._accountId);

            return Task.FromResult(true);
        }

    }
}
