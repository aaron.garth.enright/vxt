﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Shared
{
    public class DeviceStatus
    {
        public long    screenId { get; set; }
        public DateTime lastPresentationTime { get; set; }
        public DateTime nextPresentationTime { get; set; }
        public DateTime lastAdvertisement { get; set; }
        public string mainSerialNumber { get; set; }
        public string ethernetMAC { get; set; }
        public string wirelessMAC { get; set; }
        public double cpuUsage { get; set; }
        public double memoryUsage { get; set; }
        public double diskUsage { get; set; }
        public bool videoActive { get; set; }
        public bool currentlyPresenting { get; set; }
    }
}
