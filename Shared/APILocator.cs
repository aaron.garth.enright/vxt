﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Shared
{
    public class APILocator
    {
        public const string APILocation = "APILocation";

        public string Address { get; set; }
    }
}
