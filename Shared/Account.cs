﻿using MySqlX.XDevAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;

namespace RoosterOil.Shared
{
    public class Account : DbDocument, INormalizedDbDocument<Account>, IAPIAccessible
    {
        public const string apiLocation = "api/data/account";

        public string name { get; set; }
        public string number { get; set; }
        public bool isActive { get; set; }
        public Location location { get; set; }

        public List<Venue> venueList { get; set; }
        public List<EndUser> userlist { get; set; }
        public List<Creative> creatives { get; set; }
        public List<ContentFeed> contentFeeds { get; set; }
        public List<ContentFeedLoop> contentFeedLoops { get; set; }
        public List<Schedule> schedules { get; set; }

        public Account()
        {
            venueList = new List<Venue>();
            userlist = new List<EndUser>();
            creatives = new List<Creative>();
            contentFeeds = new List<ContentFeed>();
            contentFeedLoops = new List<ContentFeedLoop>();
            schedules = new List<Schedule>();
        }

        public Task<Account> Normalize()
        {
            return Task.FromResult(new Account()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                name = this.name,
                number = this.number,
                isActive = this.isActive,
                location = this.location,
                creatives = null,
                userlist = null,
                venueList = null,
                contentFeeds = null,
                contentFeedLoops = null,
                schedules = null
            });
        }

        protected override bool HasRequiredFields()
        {
            if (String.IsNullOrEmpty(this.name))
            {
                throw new DocumentDbException("Account name cannot be empty.");
            }

            if (String.IsNullOrEmpty(this.number))
            {
                throw new DocumentDbException("Account number cannot be empty.");
            }

            if (String.IsNullOrEmpty(this.location.primaryPhoneNumber))
            {
                throw new DocumentDbException("Account primary phone number cannot be empty.");
            }

            return true;
        }

        protected override async Task<bool> IsUnique(Schema schema)
        {
            if (await DocumentDbQuery.OtherExists<Account>(schema, this._id.ToLookupKey(), nameof(Account.name), this.name).ConfigureAwait(false))
            {
                throw new DocumentDbException("Attempt to create duplicate account (name in use).");
            }

            if (await DocumentDbQuery.OtherExists<Account>(schema, this._id.ToLookupKey(), nameof(Account.number), this.number).ConfigureAwait(false))
            {
                throw new DocumentDbException("Attempt to create duplicate account (account number in use).");
            }

            return true;
        }

        protected override Task<bool> PostWrite()
        {
            if (venueList != null)
            {
                foreach (var venue in venueList)
                {
                    venue._accountId = this._id;
                }
            }

            if (userlist != null)
            {
                foreach (var user in userlist)
                {
                    user._accountId = this._id;
                }
            }

            if (creatives != null)
            {
                foreach (var creative in creatives)
                {
                    creative._accountId = this._id;
                }
            }

            if (contentFeeds != null)
            {
                foreach (var contentFeed in contentFeeds)
                {
                    contentFeed._accountId = this._id;
                }
            }

            if (contentFeedLoops != null)
            {
                foreach (var contentFeedLoop in contentFeedLoops)
                {
                    contentFeedLoop._accountId = this._id;
                }
            }

            if (schedules != null)
            {
                foreach (var schedule in schedules)
                {
                    schedule._accountId = this._id;
                }
            }

            return Task.FromResult(true);
        }

        protected override async Task<bool> DeleteChildren(Schema db, LookupKey parentKey)
        {
            await DocumentDbQuery.DeleteItemsMatching<Schedule>(db, nameof(Schedule._accountId), parentKey._id).ConfigureAwait(false);
            await DocumentDbQuery.DeleteItemsMatching<Venue>(db, nameof(Venue._accountId), parentKey._id).ConfigureAwait(false);
            await DocumentDbQuery.DeleteItemsMatching<EndUser>(db, nameof(EndUser._accountId), parentKey._id).ConfigureAwait(false);
            await DocumentDbQuery.DeleteItemsMatching<ContentFeedLoop>(db, nameof(ContentFeedLoop._accountId), parentKey._id).ConfigureAwait(false);
            await DocumentDbQuery.DeleteItemsMatching<ContentFeed>(db, nameof(ContentFeed._accountId), parentKey._id).ConfigureAwait(false);
            await DocumentDbQuery.DeleteItemsMatching<Creative>(db, nameof(Creative._accountId), parentKey._id).ConfigureAwait(false);

            return true;
        }
    }
}
