﻿namespace RoosterOil.Shared
{
    public enum ContentType
    {
        blankPage = 0,
        messagePage = 1,
        adSlot = 2,
        staticVideo = 3,
        staticImage = 4,
        twitterTimeline = 5,
        streamingVideo = 6,
        weatherInfo = 7
    }
}
