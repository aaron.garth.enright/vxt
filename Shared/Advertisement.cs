﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;


namespace RoosterOil.Shared
{
    public class Advertisement : DbDocument, INormalizedDbDocument<Advertisement>, IAPIAccessible
    {
        public const string apiLocation = "api/data/advertisement";

        public int numberOfPlays { get; set; }
        [JsonIgnore]
        public byte[] adData { get; set; }
        public string fileName { get; set; }
        public string fileEncoding { get; set; }
        public string locationData { get; set; }
        public string locatorInfo { get; set; }

        public Task<Advertisement> Normalize()
        {
            return Task.FromResult(new Advertisement()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                numberOfPlays = this.numberOfPlays,
                adData = null,
                fileName = this.fileName,
                fileEncoding = this.fileEncoding,
                locationData = this.locationData,
                locatorInfo = this.locatorInfo
            });
        }
    }
}
