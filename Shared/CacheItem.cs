﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Shared
{
    public class CacheItem
    {
        public string fileName { get; set; }
        public string fileEncoding { get; set; }
        public string fileDataAsBase64 { get; set; }
    }
}
