﻿namespace RoosterOil.Shared
{
    public class ContentFeedItem
    {
        public string _contentFeedId { get; set; }
        public string _creativeId { get; set; }
        public string _backgroundImageId { get; set; }
        public ContentType type { get; set; }
        public string addressData { get; set; }
        public string addressParameters { get; set; }
        public double density { get; set; }
        public Creative creativeContent { get; set; }
        public bool fullScreen { get; set; }
        public UnitPair displayAsSize { get; set; }
        public UnitPair canvasLocation { get; set; }
        public string backgroundPaint { get; set; }
        public Creative backgroundImage { get; set; }
    }
}
