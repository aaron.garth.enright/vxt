﻿using MySqlX.XDevAPI;
using System.Collections.Generic;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;

namespace RoosterOil.Shared
{
    public class Venue : DbDocument, INormalizedDbDocument<Venue>, IAPIAccessible
    {
        public const string apiLocation = "api/data/venue";

        public string _accountId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isActive { get; set; }
        public Location location { get; set; }
        public List<Device> devicesAtLocation { get; set; }
        public GPSCoordinate venueLocation { get; set; }
        public List<VenueTags> tags { get; set; }

        public Venue()
        {
            devicesAtLocation = new List<Device>();
            tags = new List<VenueTags>();
        }

        protected override bool HasRequiredFields()
        {
            if (this._accountId == "")
            {
                throw new DocumentDbException("Cannot add venue with no account Id.");
            }

            if (this.name == "")
            {
                throw new DocumentDbException("Cannot add unnamed venue.");
            }

            if (this.venueLocation.Empty())
            {
                throw new DocumentDbException("Venue must have GPS coordinates set.");
            }

            return true;
        }

        protected override async Task<bool> IsUnique(Schema db)
        {
            var venues = await DocumentDbQuery.GetItemsMatching<Venue>(db, nameof(Venue._accountId), _accountId).ConfigureAwait(false);

            foreach (var venue in venues)
            {
                if (venue.name == this.name && venue._id != this._id)
                {
                    throw new DocumentDbException($"Cannot enter duplicate venue name {this.name} under account {this._accountId}");
                }
            }

            return true;
        }

        protected override Task<bool> PostWrite()
        {
            if (devicesAtLocation != null)
            {
                foreach (var device in devicesAtLocation)
                {
                    device._venueId = this._id;
                }
            }
            return Task.FromResult(true);
        }

        public Task<Venue> Normalize()
        {
            return Task.FromResult(new Venue()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                _accountId = this._accountId,
                name = this.name,
                description = this.description,
                isActive = this.isActive,
                location = this.location,
                devicesAtLocation = null,
                venueLocation = this.venueLocation
            });
        }

        protected override async Task<bool> DeleteChildren(Schema db, LookupKey parentKey)
        {
            await DocumentDbQuery.DeleteItemsMatching<Device>(db, nameof(Device._venueId), parentKey._id).ConfigureAwait(false);

            return true;
        }

    }
}
