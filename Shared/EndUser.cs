﻿using MySqlX.XDevAPI;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;

namespace RoosterOil.Shared
{
    public enum UserRoles
    {
        readOnly,
        contentWriter,
        siteAdministrator,
        systemAdministrator
    }

    public class EndUser : DbDocument, INormalizedDbDocument<EndUser>, IAPIAccessible
    {
        public const string apiLocation = "api/data/user";

        public string _accountId { get; set; }
        public bool isActive { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string emailAddress { get; set; }
        public UserRoles userRoles { get; set; }

        protected override bool HasRequiredFields()
        {
            if (firstName == "" || lastName == "" || emailAddress == "")
            {
                throw new DocumentDbException("User records must have a first and last name and e-mail address.");
            }

            if (!Regex.IsMatch(emailAddress, @"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase))
            {
                throw new DocumentDbException("E-mail address invalid.");
            }

            return true;
        }

        protected override async Task<bool> IsUnique(Schema db)
        {
            if (await DocumentDbQuery.OtherExists<EndUser>(db, this._id.ToLookupKey(), nameof(this.emailAddress), this.emailAddress).ConfigureAwait(false))
            {
                throw new DocumentDbException("E-mail address must be unique across all accounts.");
            }

            return true;
        }


        public Task<EndUser> Normalize()
        {
            return Task.FromResult(this);
        }
    }
}
