﻿using MySqlX.XDevAPI;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;
using System;

namespace RoosterOil.Shared
{
    public class Device : DbDocument, INormalizedDbDocument<Device>, IAPIAccessible
    {
        public const string apiLocation = "api/data/device";

        public string _venueId { get; set; }
        public string deviceId { get; set; }
        public string name { get; set; }
        public string serialNumber { get; set; }
        public string ethernetMAC { get; set; }
        public string wirelessMAC { get; set; }
        public bool isActive { get; set; }
        public GPSCoordinate deviceLocation { get; set; }

        protected override bool HasRequiredFields()
        {
            if (String.IsNullOrEmpty(deviceId))
            {
                throw new DocumentDbException("Devices must have a device id.");
            }
            return true;
        }

        protected override async Task<bool> IsUnique(Schema db)
        {
            if (await DocumentDbQuery.OtherExists<Device>(db, this._id.ToLookupKey(), nameof(Device.deviceId), deviceId).ConfigureAwait(false))
            {
                throw new DocumentDbException("Device Id must be unique");
            }
            return true;
        }

        public Task<Device> Normalize()
        {
            return Task.FromResult(this);
        }
    }
}
