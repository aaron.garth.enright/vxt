﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Shared
{
    public class Location
    {
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }
        public string primaryPhoneNumber { get; set; }
        public string secondaryPhoneNumber { get; set; }
        public string email { get; set; }
        public string URL { get; set; }
    }
}
