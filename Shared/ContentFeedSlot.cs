﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Shared
{
    public class ContentFeedSlot
    {
        private class HistogramEntry
        {
            public int relativeDensity { get; set; }
            public ContentFeedItem contentFeedItem { get; set; }
        }

        private List<HistogramEntry> playHistorgram;

        private Random randomGenerator;
        public int loopPosition { get; set; }
        public bool isEnabled { get; set; }
        public int secondsToRun { get; set; }

        public string _assignedFeedId { get; set; }
        public ContentFeed assignedFeed { get; set; }

        public ContentFeedSlot()
        {
            randomGenerator = new Random(DateTime.Now.Millisecond);
        }

        public Task<ContentFeedItem> Pick()
        {
            ContentFeedItem returnItem = null;

            if (assignedFeed != null)
            {
                if (playHistorgram == null || playHistorgram.Count == 0)
                {
                    playHistorgram = new List<HistogramEntry>();
                    foreach (var assignedFeedItem in assignedFeed.contentFeedItems)
                    {
                        playHistorgram.Add(new HistogramEntry() { relativeDensity = (int)Math.Floor(assignedFeedItem.density * 10.0), contentFeedItem = assignedFeedItem });
                    }
                }
                int pick = randomGenerator.Next(0, playHistorgram.Count);
                playHistorgram[pick].relativeDensity--;
                returnItem = playHistorgram[pick].contentFeedItem;
                if (playHistorgram[pick].relativeDensity == 0)
                {
                    playHistorgram.RemoveAt(pick);
                }
            }

            return Task.FromResult(returnItem);
        }
    }
}
