﻿using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;

namespace RoosterOil.Shared
{
    public class ContentFeed : DbDocument, INormalizedDbDocument<ContentFeed>, IAPIAccessible
    {
        public const string apiLocation = "api/data/contentfeed";

        public string _accountId { get; set; }
        public string name { get; set; }
        public ContentType type { get; set; }
        public List<ContentFeedItem> contentFeedItems { get; set; }

        public ContentFeed()
        {
            contentFeedItems = new List<ContentFeedItem>();
        }

        protected override bool HasRequiredFields()
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new DocumentDbException("Content feeds must have a name.");
            }

            foreach (var contentFeedItem in contentFeedItems)
            {
                if (String.IsNullOrEmpty(contentFeedItem.addressParameters) && String.IsNullOrEmpty(contentFeedItem.addressParameters) && String.IsNullOrEmpty(contentFeedItem._creativeId))
                {
                    throw new DocumentDbException("Content feed items must contain either locator information or a creative.");
                }
            }

            return true;
        }

        protected override async Task<bool> IsUnique(Schema db)
        {
            var contentFeeds = await DocumentDbQuery.GetItemsMatching<ContentFeed>(db, nameof(ContentFeed.name), name).ConfigureAwait(false);

            foreach (var contentFeed in contentFeeds)
            {
                if (this._accountId == contentFeed._accountId && this._id != contentFeed._id)
                {
                    throw new DocumentDbException("Content feeds must have a unique name.");
                }
            }

            return true;
        }

        public Task<ContentFeed> Normalize()
        {
            var contentFeed = new ContentFeed()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                _accountId = this._accountId,
                name = this.name,
                type = this.type
            };

            foreach (var contentFeedItem in contentFeedItems)
            {
                contentFeed.contentFeedItems.Add(new ContentFeedItem()
                {
                    _contentFeedId = contentFeedItem._contentFeedId,
                    _creativeId = (String.IsNullOrEmpty(contentFeedItem._creativeId) && contentFeedItem.creativeContent != null) ? contentFeedItem.creativeContent._id : contentFeedItem._creativeId,
                    _backgroundImageId = (String.IsNullOrEmpty(contentFeedItem._backgroundImageId) && contentFeedItem.backgroundImage != null) ? contentFeedItem.backgroundImage._id : contentFeedItem._backgroundImageId,
                    type = contentFeedItem.type,
                    addressData = contentFeedItem.addressData,
                    addressParameters = contentFeedItem.addressParameters,
                    density = contentFeedItem.density,
                    creativeContent = null,
                    fullScreen = contentFeedItem.fullScreen,
                    backgroundPaint = contentFeedItem.backgroundPaint,
                    canvasLocation = contentFeedItem.canvasLocation,
                    displayAsSize = contentFeedItem.displayAsSize,
                    backgroundImage = null
                });
            }

            return Task.FromResult(contentFeed);
        }

        protected override Task<bool> PreWrite()
        {
            foreach (var contentFeedItem in contentFeedItems)
            {
                if (contentFeedItem.creativeContent != null)
                {
                    contentFeedItem._creativeId = contentFeedItem.creativeContent._id;
                }

                if (contentFeedItem.backgroundImage != null)
                {
                    contentFeedItem._backgroundImageId = contentFeedItem.backgroundImage._id;
                }
            }

            return Task.FromResult(true);
        }

        protected override async Task<bool> DeleteChildren(Schema db, LookupKey parentKey)
        {
            foreach (var item in contentFeedItems)
            {
                if (item.creativeContent != null)
                {
                    await DbDocument.Delete<Creative>(db, item.creativeContent).ConfigureAwait(false);
                }
            }

            return true;
        }
    }
}
