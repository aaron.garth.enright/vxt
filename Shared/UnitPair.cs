﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Shared
{
    /// <summary>
    /// UnitPair:  used for screen coordinates or dimensions.  Allows specifying the dimension PLUS the unit (e.g. em, px, vh)
    /// </summary>
    public class UnitPair
    {
        public int Item1 { get; set; }
        public int Item2 { get; set; }
        public string Units { get; set; }

        public UnitPair()
        {
            Item1 = int.MinValue;
            Item2 = int.MinValue;
            Units = String.Empty;
        }

        public UnitPair(int item1, int item2, string units)
        {
            Item1 = item1;
            Item2 = item2;
            Units = units;
        }

        protected string ConvertSpecialUnits() => (Units == "vw") ? "vh" : Units;

        public UnitPair(string item1, string item2)
        {
            string firstNum = "";
            string secondNum = "";
            int unitStartsAt = 0;

            foreach (var c in item1)
            {
                if (!Char.IsDigit(c))
                {
                    break;
                }
                unitStartsAt++;
            }
            var unitName = item1.Substring(unitStartsAt);

            firstNum = item1.Substring(0, item1.Length - unitName.Length);
            secondNum = item2.Substring(0, item2.Length - unitName.Length);

            int num1 = 0;
            int num2 = 0;

            if (!int.TryParse(firstNum, out num1) || !int.TryParse(secondNum, out num2))
            {
                throw new ArgumentException("Unit pair items must be numeric and have a unit (e.g. \"px\").");
            }

            Item1 = num1;
            Item2 = num2;
            Units = unitName;
        }

        //
        //  OK, bogus?  Convenience items to use the unit pair as either a size or a point
        //
        [JsonIgnore]
        public string X => $"{Item1.ToString()}{Units}";
        [JsonIgnore]
        public string Y => $"{Item2.ToString()}{ConvertSpecialUnits()}";
        [JsonIgnore]
        public string Width => $"{Item1.ToString()}{Units}";
        [JsonIgnore]
        public string Height => $"{Item2.ToString()}{ConvertSpecialUnits()}";

        public static UnitPair FromPoint(Point xy, string units)
        {
            return new UnitPair(xy.X, xy.Y, units);
        }

        public static UnitPair FromSize(Size sz, string units)
        {
            return new UnitPair(sz.Width, sz.Height, units);
        }

        public static UnitPair FromString(string formattedString)
        {
            var parsedString = formattedString;

            if (parsedString.StartsWith('('))
            {
                if (!parsedString.EndsWith(')'))
                {
                    throw new ArgumentException("Improperly formatted UnitPair string");
                }

                parsedString = parsedString.Substring(1, parsedString.Length - 2);
            }

            string[] items = parsedString.Split(',');

            if (items.Length != 2)
            {
                throw new ArgumentException("Improperly formatted UnitPair string");
            }

            return new UnitPair(items[0], items[1]);
        }

        public bool IsValidSize()
        {
            if (Item1 <= 0 || Item2 <= 0 || String.IsNullOrEmpty(Units))
            {
                return false;
            }

            return true;
        }
    }
}
