﻿using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;

namespace RoosterOil.Shared
{
    public class Schedule : DbDocument, INormalizedDbDocument<Schedule>, IAPIAccessible
    {
        public const string apiLocation = "api/data/schedule";

        public string _accountId { get; set; }
        public string _contentFeedLoopId { get; set; }
        public List<string> _deviceIds { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }

        public DateTime lastModifiedTime { get; set; }
        public ContentFeedLoop contentFeedLoop { get; set; }
        public List<Device> deviceList { get; set; }

        public Schedule()
        {
            _deviceIds = new List<string>();
            deviceList = new List<Device>();
        }

        public async Task Load(Schema db)
        {
            lastModifiedTime = _modifiedTime;
            contentFeedLoop = await DocumentDbQuery.Get<ContentFeedLoop>(db, _contentFeedLoopId).ConfigureAwait(false);

            if (contentFeedLoop == null)
            {
                throw new DocumentDbException($"Content feed loop {_contentFeedLoopId} referenced in schedule {_id} not found.");
            }

            if (contentFeedLoop._modifiedTime > lastModifiedTime)
            {
                lastModifiedTime = contentFeedLoop._modifiedTime;
            }

            foreach (var contentFeedSlot in contentFeedLoop.contentFeedSlots)
            {
                if (!String.IsNullOrEmpty(contentFeedSlot._assignedFeedId))
                {
                    contentFeedSlot.assignedFeed = await DocumentDbQuery.Get<ContentFeed>(db, contentFeedSlot._assignedFeedId).ConfigureAwait(false);
                    if (contentFeedSlot.assignedFeed == null)
                    {
                        throw new DocumentDbException($"Content feed {contentFeedSlot._assignedFeedId} referenced in schedule {_id} not found.");
                    }

                    if (contentFeedSlot.assignedFeed._modifiedTime > lastModifiedTime)
                    {
                        lastModifiedTime = contentFeedSlot.assignedFeed._modifiedTime;
                    }

                    foreach (var contentFeedItem in contentFeedSlot.assignedFeed.contentFeedItems)
                    {
                        if (!String.IsNullOrEmpty(contentFeedItem._creativeId))
                        {
                            contentFeedItem.creativeContent = await DocumentDbQuery.Get<Creative>(db, contentFeedItem._creativeId).ConfigureAwait(false);
                            if (contentFeedItem.creativeContent == null)
                            {
                                throw new DocumentDbException($"Creative {contentFeedItem._creativeId} referenced in schedule {_id} not found.");
                            }

                            if (contentFeedItem.creativeContent._modifiedTime > lastModifiedTime)
                            {
                                lastModifiedTime = contentFeedItem.creativeContent._modifiedTime;
                            }
                        }

                        if (!String.IsNullOrEmpty(contentFeedItem._backgroundImageId))
                        {
                            contentFeedItem.backgroundImage = await DocumentDbQuery.Get<Creative>(db, contentFeedItem._backgroundImageId).ConfigureAwait(false);
                            if (contentFeedItem.backgroundImage == null)
                            {
                                throw new DocumentDbException($"Background image {contentFeedItem._backgroundImageId} referenced in schedule {_id} not found.");
                            }

                            if (contentFeedItem.backgroundImage._modifiedTime > lastModifiedTime)
                            {
                                lastModifiedTime = contentFeedItem.backgroundImage._modifiedTime;
                            }
                        }

                    }
                }
            }
        }

        protected override Task<bool> PreWrite()
        {
            if ((contentFeedLoop?._id ?? "") != "")
            {
                this._contentFeedLoopId = contentFeedLoop._id;
            }
            foreach (var device in deviceList)
            {
                _deviceIds.Add(device._id);
            }
            return Task.FromResult(true);
        }

        public Task<Schedule> Normalize()
        {
            return Task.FromResult(new Schedule()
            {
                _id = this._id,
                _createdTime = this._createdTime,
                _modifiedTime = this._modifiedTime,
                _accountId = this._accountId,
                _contentFeedLoopId = this._contentFeedLoopId,
                _deviceIds = this._deviceIds,
                deviceList = null,
                startTime = this.startTime,
                endTime = this.endTime
            });
        }
    }
}
