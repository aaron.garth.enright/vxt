﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace RoosterOil.Utility
{
    public static class HardwareInfo
    {
        public static string GetMotherBoardSerialNumber()
        {
            string serialNumber = "AEAEAEAEAEAEAEAE";

            if ((RuntimeInformation.ProcessArchitecture == Architecture.Arm ||
                RuntimeInformation.ProcessArchitecture == Architecture.Arm64)
                && RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                using (var fileVariable = new StreamReader(File.OpenRead("/proc/cpuinfo")))
                {
                    var fileText = fileVariable.ReadToEnd();

                    foreach (var line in fileText.Split('\n'))
                    {
                        if (line.StartsWith("Serial"))
                        {
                            string[] tokens = line.Split(':');

                            if (tokens.Length > 1)
                            {
                                serialNumber = tokens[1].Trim().ToUpper();
                            }
                        }
                    }
                }
            }

            return serialNumber;
        }

        public static List<Tuple<string, string>> GetMacAddresses()
        {
            var returnList = new List<Tuple<string, string>>();

            IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            if (nics == null || nics.Length < 1)
            {
                return returnList;
            }

            foreach (NetworkInterface adapter in nics)
            {
                IPInterfaceProperties properties = adapter.GetIPProperties();

                if (adapter.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                {
                    var name = adapter.Description;
                    string macAddress = "";

                    PhysicalAddress address = adapter.GetPhysicalAddress();
                    byte[] bytes = address.GetAddressBytes();

                    for (int i = 0; i < bytes.Length; i++)
                    {
                        macAddress += bytes[i].ToString("X2") + ((i != bytes.Length - 1) ? "-" : "");
                    }

                    returnList.Add(new Tuple<string, string>(adapter.Description, macAddress));
                }
            }

            return returnList;
        }
    }
}
