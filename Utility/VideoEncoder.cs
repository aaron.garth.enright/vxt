﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Events;

namespace RoosterOil.Utility
{
    public class VideoEncoder
    {
        private VideoEncoder()
        {

        }

        private readonly string _extension;
        private readonly int _minWidth;
        private readonly int _minHeight;
        private readonly ConversionProgressEventHandler _progressDelegate;
        private readonly ILogger _logger;

        public VideoEncoder(VideoEncoderOptions options, ConversionProgressEventHandler progressDelegate = null, ILogger logger = null)
        {
            FFmpeg.SetExecutablesPath(options.EncoderPath, options.EncoderName, options.ProbeName);
            _extension = options.EncodeExtension.StartsWith('.') ? options.EncodeExtension : "." + options.EncodeExtension;
            _minHeight = options.MinHeight;
            _minWidth = options.MinWidth;
            _progressDelegate = progressDelegate;
            _logger = logger;
        }

        public async Task<Size> GetSize(string inputFileName)
        {
            var mediaInfo = await FFmpeg.GetMediaInfo(inputFileName).ConfigureAwait(false);
            var videoStream = mediaInfo.VideoStreams.FirstOrDefault();

            return new Size(videoStream.Width, videoStream.Height);
        }

        public string TargetEncoding()
        {
            return _extension;
        }

        public async Task<string> ReEncode(string inputFileName, string outputFileName, bool forceResize = false)
        {
            string outputFile = outputFileName;

            var mediaInfo = await FFmpeg.GetMediaInfo(inputFileName).ConfigureAwait(false);
            var videoStream = mediaInfo.VideoStreams.FirstOrDefault();
            var audioStream = mediaInfo.AudioStreams.FirstOrDefault();

            if (_logger != null)
            {
                _logger.LogInformation($"Original video size: {videoStream.Width}x{videoStream.Height}");
                _logger.LogInformation($"Original pixel format: {videoStream.PixelFormat}");
            }

            if (Path.GetExtension(outputFileName) != $"{_extension}")
            {
                outputFile = Path.ChangeExtension(outputFileName, _extension);
            }

            videoStream
                .SetCodec(VideoCodec.h264);

            if (forceResize)
            {
                videoStream
                    .SetSize(VideoSize.Hd1080);
            }

            var conversion = new Conversion()
                .AddStream(videoStream)
                .SetOutput(outputFile)
                .UseMultiThread(true)
                .SetOverwriteOutput(true)
                .SetPreset(ConversionPreset.Medium);

            if (audioStream != null)
            {
                conversion.AddStream(audioStream);
            }

            if (_progressDelegate != null)
            {
                conversion.OnProgress += (sender, args) => _progressDelegate(sender, args);
            }

            await conversion.Start().ConfigureAwait(false);

            return outputFile;
        }
    }
}
