﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.Utility
{
    public class VideoEncoderOptions
    {
        public const string VideoReEncoder = "VideoEncoder";
        public string EncoderPath { get; set; }
        public string EncoderName { get; set; }
        public string ProbeName { get; set; }
        public int MinWidth { get; set; }
        public int MinHeight { get; set; }
        public string EncodeExtension { get; set; }
    }
}
