﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;

namespace RoosterOil.Utility
{
    public class RemoteSystem
    {
        private static ConcurrentDictionary<string, SemaphoreSlim> semaphores = new ConcurrentDictionary<string, SemaphoreSlim>();

        private readonly RemoteSystemOptions _connection;

        private RemoteSystem() {}

        public RemoteSystem(RemoteSystemOptions connectionInfo)
        {
            _connection = connectionInfo;
        }

        public int ExecuteCommandAsync(string programFile, string programArguments)
        {
            int returnValue = 0;

            try
            {
                using (var processImage = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        Arguments = programArguments,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        FileName = programFile,
                    }
                })
                {
                    processImage.Start();
                    processImage.WaitForExit();
                    returnValue = processImage.ExitCode;
                }
            }
            catch (Exception)
            {
            }

            return returnValue;
        }

        public int CreateDirectory(string directoryName)
        {
            return ExecuteCommandAsync(_connection.SSHCommand, $"-i {_connection.KeyFile} {_connection.User}@{_connection.System} " + "\"mkdir -p " + directoryName + "\"");
        }

        public int DestroyDirectory(string directoryName)
        {
            return ExecuteCommandAsync(_connection.SSHCommand, $"-i {_connection.KeyFile} {_connection.User}@{_connection.System} " + "\"rm -r " + directoryName + "/* ; rmdir " + directoryName + "\"");
        }

        public int CopyTo(string localFileName, string remoteDirectory)
        {
            return ExecuteCommandAsync(_connection.SCPCommand, $"-rp -i {_connection.KeyFile} {localFileName} {_connection.User}@{_connection.System}:{remoteDirectory}");
        }

        public int CopyFrom(string remoteFileName, string localDirectory)
        {
            return ExecuteCommandAsync(_connection.SCPCommand, $"-rp -i {_connection.KeyFile} {_connection.User}@{_connection.System}:{remoteFileName} {localDirectory}");
        }

        public string HomeDirectory()
        {
            return $"/home/{_connection.User}/";
        }

    }
}
