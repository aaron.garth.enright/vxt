﻿using Microsoft.AspNetCore.StaticFiles;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace RoosterOil.Utility
{
    public static class FileData
    {
        public static string Checksum(byte[] buffer)
        {
            return Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(buffer));
        }

        public static string ToDisplay(byte[] buffer, string encoding)
        {
            return $"data:{encoding};base64,{Convert.ToBase64String(buffer)}";
        }

        private static async Task<string> FileCheckSumInternal(string fileName, long length)
        {
            var buffer = new byte[length];

            using (var streamReader = File.OpenRead(fileName))
            {
                await streamReader.ReadAsync(buffer, 0, (int)length);
                streamReader.Close();
            }
            return Checksum(buffer);
        }

        public static async Task<string> FileChecksum(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            return await FileCheckSumInternal(fileName, fileInfo.Length).ConfigureAwait(false);
        }

        public static async Task<bool> CompareChecksum(string fileName, long length, string checkSum)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            if (fileInfo.Length != length || checkSum != await FileCheckSumInternal(fileName, length))
            {
                return false;
            }

            return true;
        }

        public static string GetContentTypeFromMIMETable(string fileName)
        {
            string contentType;
            if (!new FileExtensionContentTypeProvider().TryGetContentType(fileName, out contentType))
            {
                contentType = "application/octet-stream";
            }

            return contentType;
        }
    }
}
