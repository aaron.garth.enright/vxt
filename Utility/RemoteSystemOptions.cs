﻿namespace RoosterOil.Utility
{
    public class RemoteSystemOptions
    {
        public const string RemoteSystem = "RemoteSystem";
        public string System { get; set; }
        public string User { get; set; }
        public string KeyFile { get; set; }
        public string SSHCommand { get; set; }
        public string SCPCommand { get; set; }
    }
}
