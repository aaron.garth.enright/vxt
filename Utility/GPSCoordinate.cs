﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.Utility
{
    public class GPSCoordinate
    {
        public GPSCoordinate()
        {
            latitude = double.NaN;
            longitude = double.NaN;
        }

        public GPSCoordinate(double lat, double lon)
        {
            latitude = lat;
            longitude = lon;
        }

        public override string ToString()
        {
            return $"{latitude.ToString("0.00000")},{longitude.ToString("0.00000")}";
        }

        double latitude { get; set; }
        double longitude { get; set; }

        public bool Empty()
        {
            return (latitude == double.NaN || longitude == double.NaN);
        }
    }
}
