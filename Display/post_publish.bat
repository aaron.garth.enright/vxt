@echo off
if exist Display.tar.gz (
   del Display.tar.gz
)

tar cvfz Display.tar.gz %DISPLAY_DIR%
copy /y Display.tar.gz %PUBLISH_DIR%
pause