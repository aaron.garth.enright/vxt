﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RoosterOil.DisplayAPI.Client;
using RoosterOil.Shared;
using RoosterOil.Utility;

namespace RoosterOil.Display.Data
{
    public class DisplayService
    {
        private static Schedule currentSchedule = null;
        private readonly ILogger<DisplayService> _logger;
        private readonly APILocator _apiURL;
        private readonly DisplayInfo _displayInfo;

        private int currentSlot = 0;
        public DisplayService(ILogger<DisplayService> logger, IOptions<APILocator> apiURL, IOptions<DisplayInfo> options)
        {
            _logger = logger;
            _apiURL = apiURL.Value;
            _displayInfo = options.Value;
        }

        public static void SetSchedule(Schedule newSchedule)
        {
            currentSchedule = newSchedule;
        }

        protected DisplayBackground AddBackgroundInformation(ContentFeedItem contentFeedItem, string name)
        {
            if (contentFeedItem.fullScreen == true)
            {
                return null;
            }
            var backgroundInfo = new DisplayBackground();

            if (contentFeedItem.canvasLocation != null)
            {
                backgroundInfo.canvasLocation = contentFeedItem.canvasLocation;
            }

            if (contentFeedItem.displayAsSize != null)
            {
                backgroundInfo.displayAsSize = contentFeedItem.displayAsSize;
            }
            else if (contentFeedItem.creativeContent != null)
            {
                backgroundInfo.displayAsSize = contentFeedItem.creativeContent.originalResolution;
            }

            if (contentFeedItem.backgroundImage != null)
            {
                backgroundInfo.backgroundImage = $"{_displayInfo.BrowserCachePath}/{contentFeedItem.backgroundImage.CreativeFileName}";
            }

            if (!String.IsNullOrWhiteSpace(contentFeedItem.backgroundPaint))
            {
                backgroundInfo.backgroundPaint = contentFeedItem.backgroundPaint;
            }

            if (!backgroundInfo.displayAsSize.IsValidSize())
            {
                _logger.LogError($"Content feed item {name} has no display dimensions.  Will display full screen.");

                return null;
            }
            return backgroundInfo;
        }

        public async Task<DisplayItem> GetNextDisplayItemAsync()
        {
            _logger.LogInformation($"Polling for Display updates.  Current schedule is {(currentSchedule == null ? "none" : currentSchedule._id)}.");
            var contentItemToDisplay = new DisplayItem();
            if (currentSchedule != null && currentSchedule.endTime > DateTime.Now)
            {
                if (currentSchedule.startTime <= DateTime.Now && currentSchedule.endTime >= DateTime.Now)
                {
                    var contentFeedSlot = currentSchedule.contentFeedLoop.contentFeedSlots[currentSlot];
                    currentSlot = ++currentSlot % currentSchedule.contentFeedLoop.contentFeedSlots.Count;
                    var contentFeedItem = await contentFeedSlot.Pick();

                    contentItemToDisplay.fullScreen = contentFeedItem.fullScreen;

                    contentItemToDisplay.itemType = contentFeedSlot.assignedFeed.type;
                    contentItemToDisplay.secondsToRun = contentFeedSlot.secondsToRun;
                    if (contentFeedItem.creativeContent != null)
                    {
                        contentItemToDisplay.locatorData = $"{_displayInfo.BrowserCachePath}/{contentFeedItem.creativeContent.CreativeFileName}";
                    }
                    else
                    {
                        contentItemToDisplay.locatorData = contentFeedItem.addressData;
                        contentItemToDisplay.locatorInfo = contentFeedItem.addressParameters;
                    }

                    if (!contentItemToDisplay.fullScreen)
                    {
                        contentItemToDisplay.background = AddBackgroundInformation(contentFeedItem, contentItemToDisplay.locatorData);
                    }

                    _logger.LogInformation($"Next item to display: {contentItemToDisplay.locatorData}");
                }
                else if (currentSchedule.startTime > DateTime.Now)
                {
                    _logger.LogInformation($"Next presentation will start at {currentSchedule.startTime}");
                    contentItemToDisplay.itemType = ContentType.messagePage;
                    contentItemToDisplay.locatorData = $"Next Presentation at {currentSchedule.startTime}";
                    if (currentSchedule.startTime - DateTime.Now < TimeSpan.FromSeconds(30))
                    {
                        contentItemToDisplay.secondsToRun = (int)Math.Round((currentSchedule.startTime - DateTime.Now).TotalSeconds);
                    }
                    else
                    {
                        contentItemToDisplay.secondsToRun = 30;
                    }
                }
            }
            else
            {
                contentItemToDisplay.itemType = ContentType.messagePage;
                contentItemToDisplay.locatorData = "No Upcoming Presentations Scheduled.";
                contentItemToDisplay.secondsToRun = 5;
            }
            return contentItemToDisplay;
        }
    }
}
