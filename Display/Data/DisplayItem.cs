﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RoosterOil.Shared;

namespace RoosterOil.Display.Data
{
    public class DisplayItem
    {
        public ContentType itemType { get; set; }
        public string locatorData { get; set; }
        public string locatorInfo { get; set; }
        public int secondsToRun { get; set; }
        public bool fullScreen { get; set; }

        public DisplayBackground background { get; set; }
    }
}
