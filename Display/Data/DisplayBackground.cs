﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using RoosterOil.Shared;

namespace RoosterOil.Display.Data
{
    public class DisplayBackground
    {
        public UnitPair displayAsSize { get; set; }
        public UnitPair canvasLocation { get; set; }
        public string backgroundPaint { get; set; }
        public string backgroundImage { get; set; }
    }
}
