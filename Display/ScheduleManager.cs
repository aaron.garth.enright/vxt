﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Threading;
using RoosterOil.DisplayAPI.Client;
using RoosterOil.Display.Data;
using RoosterOil.Shared;
using RoosterOil.Utility;

namespace RoosterOil.Display
{
    public class ScheduleManager : BackgroundService
    {
        private readonly ILogger<ScheduleManager> _logger;
        private readonly APILocator _APIaddress;
        private readonly DisplayInfo _options;

        private readonly Device localDeviceInfo;

        public ScheduleManager(ILogger<ScheduleManager> logger, IOptions<APILocator> APIaddress, IOptions<DisplayInfo> options)
        {
            _logger = logger;
            _APIaddress = APIaddress.Value;
            _options = options.Value;

            var macAddresses = HardwareInfo.GetMacAddresses();

            localDeviceInfo = new Device()
            {
                isActive = true,
                deviceId = _options.DeviceId,
                serialNumber = HardwareInfo.GetMotherBoardSerialNumber(),
                deviceLocation = new GPSCoordinate(_options.Latitude, _options.Longitude),
                ethernetMAC = (from mac in macAddresses where mac.Item1 == "eth0" select mac.Item2).FirstOrDefault(),
                wirelessMAC = (from mac in macAddresses where mac.Item1 == "wlan0" select mac.Item2).FirstOrDefault(),
            };
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Assembly thisAssem = typeof(ScheduleManager).Assembly;
            AssemblyName thisAssemName = thisAssem.GetName();

            Version ver = thisAssemName.Version;

            _logger.LogInformation($"{thisAssemName} version {ver}."); 

            try
            {
                _logger.LogInformation($"Schedule monitor thread started for device {localDeviceInfo.deviceId}.");
                var client = new DeviceClient(_APIaddress.Address);

                var lastUpdated = DateTime.MinValue;
                Schedule currentSchedule = null;

                while (!stoppingToken.IsCancellationRequested)
                {
                    try
                    {
                        var workloadResponse = await client.GetScheduleChanges(localDeviceInfo.deviceId, (currentSchedule == null) ? "(null)" : currentSchedule._id, lastUpdated).ConfigureAwait(false);

                        if (workloadResponse != null)
                        {
                            lastUpdated = workloadResponse.responseTime;
                            if (workloadResponse.newSchedule != null)
                            {
                                _logger.LogInformation($"Recieved schedule update for {workloadResponse.newSchedule._id}");
                                currentSchedule = workloadResponse.newSchedule;
                                foreach (var contentFeedSlot in currentSchedule?.contentFeedLoop.contentFeedSlots)
                                {
                                    if (contentFeedSlot?.assignedFeed != null)
                                    {
                                        foreach (var contentFeedItem in contentFeedSlot.assignedFeed.contentFeedItems)
                                        {
                                            if (contentFeedItem.creativeContent != null)
                                            {
                                                if (!await contentFeedItem.creativeContent.CacheUpToDate(_options.ServerCachePath).ConfigureAwait(false))
                                                {
                                                    _logger.LogInformation($"Creating local copy of creative {contentFeedItem.creativeContent._id} ({contentFeedItem.creativeContent.name})");
                                                    await client.GetCreative(contentFeedItem.creativeContent, _options.ServerCachePath).ConfigureAwait(false);
                                                }
                                            }

                                            if (contentFeedItem.backgroundImage != null)
                                            {
                                                if (!await contentFeedItem.backgroundImage.CacheUpToDate(_options.ServerCachePath).ConfigureAwait(false))
                                                {
                                                    _logger.LogInformation($"Creating local copy of background {contentFeedItem.backgroundImage._id} ({contentFeedItem.backgroundImage.name})");
                                                    await client.GetCreative(contentFeedItem.backgroundImage, _options.ServerCachePath).ConfigureAwait(false);
                                                }
                                            }

                                        }
                                    }
                                }

                                DisplayService.SetSchedule(currentSchedule);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Exception caught (monitor still alive):\r\n{ex.Message}");
                    }

                    await Task.Delay(_options.PollingFrequency * 1000);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception caught (MONITOR DEAD):\r\n{ex.Message}");
            }

            _logger.LogInformation("Schedule monitor thread terminating.");
        }
    }
}
