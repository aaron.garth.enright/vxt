﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.Display
{
    static public class DatetimeExtensions
    {
        static public string BuildNumber(this DateTime input)
        {
            return new TimeSpan(input.Ticks - new DateTime(2019, 4, 11).Ticks).TotalMinutes.ToString("0");
        }
    }
}
