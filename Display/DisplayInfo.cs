﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.Display
{
    public class DisplayInfo
    {
        public const string DisplayConfig = "DisplayConfig";
        public string DeviceId { get; set; }
        public string ServerCachePath { get; set; }
        public string BrowserCachePath { get; set; }
        public int PollingFrequency { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
