﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.Display.Shared
{
    public static class RenderHelper
    {
        public static string ParseBackgroundPaint(string paint, bool getFirstColorOnly = false)
        {
            if (String.IsNullOrEmpty(paint))
            {
                return "#000000";
            }

            if (paint.Contains(':'))
            {
                string[] colors = paint.Split(':');
                return (getFirstColorOnly || colors.Length < 2) ? colors[0] : $"linear-gradient(to bottom right, {colors[0]}, {colors[1]})";
            }

            return paint;
        }

        public static string CreateStyleString(string width, string height, string X, string Y)
        {
            string styleString = "";

            if (!String.IsNullOrEmpty(X) || !String.IsNullOrEmpty(Y))
            {
                styleString = $"position: fixed; width: {width}; height: {height}; top: {(String.IsNullOrEmpty(Y) ? "0px" : Y)}; left: {(String.IsNullOrEmpty(X) ? "0px" : X)}; ";
            }
            else
            {
                styleString = $"position: fixed; top: calc(50vh - {height} / 2); left: calc(50vw - {width} / 2); width: {width}; height: {height}; ";
            }

            styleString += "object-fit: fill; cursor: none; border: none; border-style: none; outline: none;";

            return styleString;
        }
    }
}
