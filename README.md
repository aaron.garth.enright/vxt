# VXT Content Management API and Display Terminal

(For meaning of "VXT" see my development notes)

This solution contains the following projects:

1. APITester - used to test code against the client-side API (vxtCMSAPI.Client).
2. vxtCMSAPI.Client - the client side API, written in C\# and Rest\#.
3. vxtCMS - the Web API Server as well as the (eventual) CMS UI.  Blazor server using Swashbuckle to generate the swagger spec and Kestrel.
4. vxtDisplay - the display terminal and "mini-server" - a C\# Blazor Server app. with Kestrel intended to run on the Raspberry Pi.  Testable on PC.
5. vxtShared - shared C\# classes.

For vxtDisplay to communicate back to the Windows-hosted API, appropriate certificates must be created to allow SSL.  The CMS Web Server is set to run directly on port 443 of the hosting Windows system (i.e., shut down other web hosts before starting).  vxtDisplay's mini-server currently sits on port 5001 on the Pi (or PC for testing), but it should not be reachable from the internet anyways.

Notes on identity model:
1) Identity Server 4 is now hooked in, but really isn't doing anything.
2) Moved all identity repositories (ASP Net User store, IS4 configuration store, and IS4 persisted grants store) to MySQL.  However, *THERE IS CURRENTLY A BUG in MySQL Entity Framework*.  This requires us to downgrade all Microsoft Entity Framework packages to version 3.1.10 and use the Pomelo MySQL Entity Framework NUGet package.  This is really only for generating and populating the database, but we'll leave them there until either the bug is fixed or we're in production.
3) Google authentication seems to be working.  Question for the team will be who else?  (Facebook, Twitter, ???)

