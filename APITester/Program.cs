﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.DisplayAPI.Client;
using RoosterOil.ContentAPI.Client;

namespace APITester2
{
    class Program
    {
        static async Task TestDeviceAPI()
        {
            var client = new DeviceClient("https://localhost:5001");

            var workloadResponse = await client.GetScheduleChanges("66AE0520", "(null)", DateTime.MinValue);
        }

        static async Task TestContentAPI()
        {
            var client = new DataClient("https://localhost:5001");
            
            try
            {
/*                var newCreative = await Creative.CreateFromFile(@"C:\Users\Aaron\Downloads\HockeySkills.png").ConfigureAwait(false);
                newCreative._accountId = "00005fdfedfc0000000000000848";
                var test = await client.Save<Creative>(newCreative).ConfigureAwait(false);*/

                var creatives = await client.UploadCreatives("00005fdfedfc0000000000000848", new List<string>()
                {
                    @"C:\Users\Aaron\Downloads\BCH Apply now.mp4",
                    @"C:\Users\Aaron\Downloads\Good-Sport.mp4",
                    @"C:\Users\Aaron\Downloads\Softball Summer Camp - TV.png",
                    @"C:\Users\Aaron\Downloads\BSP - 1.png"
                }).ConfigureAwait(false);
                var ad = await client.CreateAdvertisement(@"C:\Users\Aaron\Downloads\adspeedlogointro.mp4", 30, 10).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                var x = ex.Message;
            }

            return;
            /*
            var account = new Account()
            {
                name = "XYZ Corporation",
                number = "1234567789",
                location = new Location()
                {
                    primaryPhoneNumber = "8008008000"
                }
            };

            var newAccount = await client.Save<Account>(account).ConfigureAwait(false);

            var reReadAccount = await client.Get<Account>(newAccount._id).ConfigureAwait(false);

            try
            {
                var badAccounts = await client.GetBy<Account>("None", "BadValue").ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var nextAccount = await client.GetAccountByNumber("1234567789").ConfigureAwait(false);

            newAccount.location.address = "1800 Main Street";
            newAccount.location.city = "Pleasantville";
            newAccount.location.state = "OH";

            await client.Update<Account>(newAccount);
            nextAccount = await client.GetAccountByNumber("1234567789").ConfigureAwait(false);

            await client.Delete<Account>(newAccount._id);*/
        }

        static async Task Main(string[] args)
        {
            await TestContentAPI();
        }
    }
}
