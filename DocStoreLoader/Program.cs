﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;

namespace DocumentDbLoader
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).Build().Services.CreateScope().ServiceProvider.GetRequiredService<Loader>().Run(args);
        }

        static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) =>
                {
                    services.Configure<DocumentDbOptions>(context.Configuration.GetSection(DocumentDbOptions.DocumentDb));
                    services.Configure<RemoteSystemOptions>(context.Configuration.GetSection(RemoteSystemOptions.RemoteSystem));
                    services.Configure<VideoEncoderOptions>(context.Configuration.GetSection(VideoEncoderOptions.VideoReEncoder));
                    services.Configure<CreativeCacheOptions>(context.Configuration.GetSection(CreativeCacheOptions.CacheInformation));
                    services.AddTransient<Loader>();
                })
                .ConfigureLogging(logging =>
                {
                    logging.SetMinimumLevel(LogLevel.Information);
                    logging.AddLog4Net("log4net.config");
                })
                .UseConsoleLifetime();

    }
}
