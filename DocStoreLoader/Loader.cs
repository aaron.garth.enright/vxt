﻿using MySqlX.XDevAPI;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;

namespace DocumentDbLoader
{
    public class Loader
    {
        private readonly ILogger _logger;

        private Task ValidateLine<T>(int tokenCount, string[] tokens)
        {
            if (tokens.Length < tokenCount)
            {
                _logger.LogError($"Invalid {typeof(T).Name} record in file, exiting!");
                throw new Exception($"Too few tokens in {typeof(T).Name} record in data file.");
            }

            return Task.CompletedTask;
        }

        private Task<DateTime> ConvertFromFileNotation(string fileDateTime)
        {
            if (!fileDateTime.StartsWith('+'))
            {
                return Task.FromResult(DateTime.Parse(fileDateTime));
            }
            else if (fileDateTime.Length > 1)
            {
                return Task.FromResult(DateTime.Now.AddSeconds(double.Parse(fileDateTime.Substring(1))));
            }

            return Task.FromResult(DateTime.Now);
        }

        private void CreateCollections(Schema db)
        {
            DbDocument.CreateCollection<Account>(db);
            DbDocument.CreateCollection<Venue>(db);
            DbDocument.CreateCollection<Device>(db);
            DbDocument.CreateCollection<EndUser>(db);
            DbDocument.CreateCollection<ContentFeed>(db);
            DbDocument.CreateCollection<Creative>(db);
            DbDocument.CreateCollection<ContentFeedLoop>(db);
            DbDocument.CreateCollection<Schedule>(db);
        }

        public Loader(ILogger<Loader> logger, IOptions<DocumentDbOptions> docStoreOptions, IOptions<RemoteSystemOptions> remoteSystemInfo, IOptions<VideoEncoderOptions> videoReEncoder,
            IOptions<CreativeCacheOptions> cacheOptions)
        {
            _logger = logger;

            DocStoreConfiguration.SetDocStoreConfiguration(_logger, docStoreOptions.Value, remoteSystemInfo.Value, videoReEncoder.Value, cacheOptions.Value);
        }

        protected async Task<string> CreateAccount(Schema db, string[] tokens)
        {
            await ValidateLine<Account>(13, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Creating account for {tokens[2]}.");

            var account = new Account()
            {
                isActive = (tokens[1] == "1") ? true : false,
                name = tokens[2],
                number = tokens[3],
                location = new Location()
                {
                    address = tokens[4],
                    city = tokens[5],
                    state = tokens[6],
                    country = tokens[7],
                    postalCode = tokens[8],
                    primaryPhoneNumber = tokens[9],
                    secondaryPhoneNumber = tokens[10],
                    email = tokens[11],
                    URL = tokens[12]
                }
            };

            await DbDocument.Create<Account>(db, account).ConfigureAwait(false);

            _logger.LogInformation($"Account id {account._id} created.");

            return account._id;
        }

        protected async Task<string> CreateVenue(Schema db, string accountId, string[] tokens)
        {
            await ValidateLine<Venue>(15, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Creating venue named {tokens[2]} under account id {accountId}.");

            var venue = new Venue()
            {
                _accountId = accountId,
                isActive = (tokens[1] == "1") ? true : false,
                name = tokens[2],
                description = tokens[3],
                location = new Location()
                {
                    address = tokens[4],
                    city = tokens[5],
                    state = tokens[6],
                    country = tokens[7],
                    postalCode = tokens[8],
                    primaryPhoneNumber = tokens[9],
                    secondaryPhoneNumber = tokens[10],
                    email = tokens[11],
                    URL = tokens[12]
                },
                venueLocation = new GPSCoordinate(Double.Parse(tokens[13]), Double.Parse(tokens[14]))
            };

            await DbDocument.Create<Venue>(db, venue).ConfigureAwait(false);

            _logger.LogInformation($"Venue id {venue._id} created.");

            return venue._id;
        }

        protected async Task<string> CreateUser(Schema db, string accountId, string[] tokens)
        {
            await ValidateLine<EndUser>(6, tokens).ConfigureAwait(false);
 
            _logger.LogInformation($"Creating user named {tokens[2]} {tokens[3]} under account id {accountId}.");

            var user = new EndUser()
            {
                _accountId = accountId,
                isActive = (tokens[1] == "1") ? true : false,
                firstName = tokens[2],
                lastName = tokens[3],
                emailAddress = tokens[4],
                userRoles = Enum.Parse<UserRoles>(tokens[5], true)
            };

            await DbDocument.Create<EndUser>(db, user).ConfigureAwait(false);

            _logger.LogInformation($"User id {user._id} created.");

            return user._id;
        }

        protected async Task<string> CreateDevice(Schema db, string venueId, string[] tokens)
        {
            await ValidateLine<Device>(7, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Creating device with named {tokens[2]} in venue {venueId}.");

            var device = new Device()
            {
                _venueId = venueId,
                isActive = (tokens[1] == "1") ? true : false,
                deviceId = tokens[2],
                name = tokens[3],
                serialNumber = tokens[4],
                ethernetMAC = tokens[5],
                wirelessMAC = tokens[6]
            };

            await DbDocument.Create<Device>(db, device).ConfigureAwait(false);

            _logger.LogInformation($"Device id {device._id} created.");

            return device._id;
        }

        protected async Task<ContentFeed> OpenContentFeed(string accountId, string[] tokens)
        {
            await ValidateLine<ContentFeed>(3, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Begin new content feed in account {accountId}.");

            return new ContentFeed()
            {
                _accountId = accountId,
                name = tokens[1],
                type = Enum.Parse<ContentType>(tokens[2], true)
            };
        }

        protected async Task<string> AddOrGetCreative(Schema db, string creativeName, string accountId)
        {
            string creativeId = "";

            var creatives = await DocumentDbQuery.GetItemsMatching<Creative>(db, nameof(Creative.name), Creative.NameFromFileName(creativeName)).ConfigureAwait(false);
            foreach (var creative in creatives)
            {
                if (creative._accountId == accountId)
                {
                    creativeId = creative._id;
                    break;
                }
            }

            if (String.IsNullOrEmpty(creativeId))
            {
                var newCreative = await Creative.CreateFromFile(creativeName).ConfigureAwait(false);
                newCreative._accountId = accountId;
                await DbDocument.Create<Creative>(db, newCreative).ConfigureAwait(false);
                creativeId = newCreative._id;
            }

            return creativeId;
        }

        protected async Task AddItemToContentFeed(Schema db, ContentFeed contentFeed, string[] tokens)
        {
            await ValidateLine<ContentFeed>(9, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Adding new content feed item {tokens[2]} {tokens[3]} {tokens[4]}.");

            string creativeId = "";

            if (!String.IsNullOrEmpty(tokens[4]))
            {
                creativeId = await AddOrGetCreative(db, tokens[4], contentFeed._accountId).ConfigureAwait(false);
            }

            bool fullSize = true;
            UnitPair displaySize = null;

            if (!String.IsNullOrEmpty(tokens[5]))
            {
                displaySize = UnitPair.FromString(tokens[5]);
                fullSize = false;
            }

            UnitPair screenPosition = null;
            if (!String.IsNullOrEmpty(tokens[6]))
            {
                screenPosition = UnitPair.FromString(tokens[6]);
                fullSize = false;
            }

            string backgroundImageId = "";

            if (!String.IsNullOrEmpty(tokens[7]))
            {
                backgroundImageId = await AddOrGetCreative(db, tokens[7], contentFeed._accountId).ConfigureAwait(false);
            }

            if (!String.IsNullOrEmpty(tokens[8]))
            {
                fullSize = false;
            }

            contentFeed.contentFeedItems.Add(new ContentFeedItem()
            {
                addressData = tokens[2],
                addressParameters = tokens[3],
                _creativeId = creativeId,
                fullScreen = fullSize,
                displayAsSize = displaySize,
                canvasLocation = screenPosition,
                _backgroundImageId = backgroundImageId,
                backgroundPaint = tokens[8]
            });
        }

        protected async Task<ContentFeed> CloseContentFeedIfOpen(Schema db, ContentFeed contentFeed)
        {
            if (contentFeed != null)
            {
                await DbDocument.Create<ContentFeed>(db, contentFeed).ConfigureAwait(false);

                _logger.LogInformation($"ContentFeed {contentFeed._id} created.");
            }
            return null;
        }

        protected async Task<ContentFeedLoop> OpenContentFeedLoop(string accountId, string[] tokens)
        {
            await ValidateLine<ContentFeedLoop>(2, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Begin new content feed loop in account {accountId}.");

            return new ContentFeedLoop()
            {
                _accountId = accountId,
                name = tokens[1]
            };
        }

        protected async Task AddSlotToContentFeedLoop(Schema db, ContentFeedLoop contentFeedLoop, string[] tokens)
        {
            await ValidateLine<ContentFeedSlot>(5, tokens).ConfigureAwait(false);

            _logger.LogInformation($"Adding new content feed slot for {tokens[4]}.");

            int position = 0;
            int seconds = 0;
            if (!int.TryParse(tokens[2], out position))
            {
                throw new Exception("Invalid loop position for slot.");
            }

            if (!int.TryParse(tokens[3], out seconds))
            {
                throw new Exception("Invalid loop position for slot.");
            }

            var contentFeedList = await DocumentDbQuery.GetKeysMatching<ContentFeed>(db, nameof(ContentFeed.name), tokens[4]).ConfigureAwait(false);

            if (contentFeedList.Count() == 0)
            {
                throw new Exception("Invalid content feed for slot.");
            }

            contentFeedLoop.contentFeedSlots.Add(new ContentFeedSlot()
            {
                isEnabled = (tokens[1] == "1") ? true : false,
                loopPosition = position,
                secondsToRun = seconds,
                _assignedFeedId = contentFeedList.FirstOrDefault()._id
            });
        }

        protected async Task<ContentFeedLoop> CloseContentFeedLoopIfOpen(Schema db, ContentFeedLoop contentFeedLoop)
        {
            if (contentFeedLoop != null)
            {
                await DbDocument.Create<ContentFeedLoop>(db, contentFeedLoop).ConfigureAwait(false);

                _logger.LogInformation($"ContentFeedLoop {contentFeedLoop._id} created.");
            }

            return null;
        }

        protected async Task<string> CreateSchedule(Schema db, string currentAccountId, string[] tokens)
        {
            await ValidateLine<Device>(6, tokens).ConfigureAwait(false);

            var accountId = currentAccountId;

            if (!String.IsNullOrEmpty(tokens[1]))
            {
                var keys = (await DocumentDbQuery.GetKeysMatching<Account>(db, nameof(Account.number), tokens[1]).ConfigureAwait(false)).FirstOrDefault();

                if (keys != null)
                {
                    accountId = keys._id;
                }
            }

            _logger.LogInformation($"Creating schedule for {accountId} on devices {tokens[2]}.");

            var deviceKeys = await DocumentDbQuery.GetKeysIn<Device>(db, nameof(Device.name), tokens[2]).ConfigureAwait(false);

            if (deviceKeys.Count() == 0)
            {
                throw new Exception("No valid devices found in schedule definition");
            }

            var contentFeedLoopKeys = await DocumentDbQuery.GetKeysMatching<ContentFeedLoop>(db, nameof(ContentFeedLoop.name), tokens[3]).ConfigureAwait(false);

            if (contentFeedLoopKeys.Count() == 0)
            {
                throw new Exception($"Cannot find a content feed loop named {tokens[3]}");
            }

            var schedule = new Schedule()
            {
                _accountId = accountId,
                _contentFeedLoopId = contentFeedLoopKeys.FirstOrDefault()._id,
                startTime = await ConvertFromFileNotation(tokens[4]),
                endTime = await ConvertFromFileNotation(tokens[5])
            };

            foreach (var device in deviceKeys)
            {
                schedule._deviceIds.Add(device._id);
            }

            await DbDocument.Create<Schedule>(db, schedule).ConfigureAwait(false);

            _logger.LogInformation($"Schedule id {schedule._id} created.");

            return schedule._id;
        }

        internal async Task Run(string[] args)
        {
            using (var streamReader = new StreamReader(File.OpenRead(args[0])))
            {
                _logger.LogInformation($"Begin parsing file {args[0]}.");
                var db = DbDocument.GetStore(_logger);
                _logger.LogInformation($"Opened document schema {DocStoreConfiguration.GetDocStoreConfiguration()._docStoreOptions.Schema} on server {DocStoreConfiguration.GetDocStoreConfiguration()._docStoreOptions.Server} as user {DocStoreConfiguration.GetDocStoreConfiguration()._docStoreOptions.User}.");

                CreateCollections(db);

                string currentAccountId = "";
                string currentVenueId = "";
                ContentFeed currentContentFeed = null;
                ContentFeedLoop currentContentFeedLoop = null;

                string line = streamReader.ReadLine();
                while (line != null)
                {
                    if (!line.StartsWith("--"))
                    {
                        string[] tokens = line.Split('|');

                        switch (tokens[0])
                        {
                            case "Account":
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                currentAccountId = await CreateAccount(db, tokens).ConfigureAwait(false);
                                break;
                            case "Venue":
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                currentVenueId = await CreateVenue(db, currentAccountId, tokens).ConfigureAwait(false);
                                break;
                            case "User":
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                await CreateUser(db, currentAccountId, tokens).ConfigureAwait(false);
                                break;
                            case "Device":
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                await CreateDevice(db, currentVenueId, tokens).ConfigureAwait(false);
                                break;
                            case "ContentFeed":
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                currentContentFeed = await OpenContentFeed(currentAccountId, tokens).ConfigureAwait(false);
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                break;
                            case "ContentFeedItem":
                                await AddItemToContentFeed(db, currentContentFeed, tokens).ConfigureAwait(false);
                                break;
                            case "ContentFeedLoop":
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                currentContentFeedLoop = await OpenContentFeedLoop(currentAccountId, tokens).ConfigureAwait(false);
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                break;
                            case "ContentFeedSlot":
                                await AddSlotToContentFeedLoop(db, currentContentFeedLoop, tokens).ConfigureAwait(false);
                                break;
                            case "Schedule":
                                currentContentFeedLoop = await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                                currentContentFeed = await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
                                await CreateSchedule(db, currentAccountId, tokens);
                                break;
                            default:
                                _logger.LogWarning("Line with unrecognized initial token.");
                                break;
                        }
                    }

                    line = streamReader.ReadLine();
                }

                await CloseContentFeedLoopIfOpen(db, currentContentFeedLoop).ConfigureAwait(false);
                await CloseContentFeedIfOpen(db, currentContentFeed).ConfigureAwait(false);
            }
        }
    }
}
