﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using RoosterOil.CMS.Models;

namespace RoosterOil.CMS.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Necessary for mySQL;  does not seem to affect anything on SqlLite or SqlServer
            builder.Entity<HistoryRow>().HasNoKey();
            builder.Entity<HistoryRow>().Property(h => h.MigrationId).HasMaxLength(100).IsRequired();
            builder.Entity<HistoryRow>().Property(h => h.ProductVersion).HasMaxLength(200).IsRequired();
        }
    }
}
