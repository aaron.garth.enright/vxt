﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;
using RoosterOil.Shared;


namespace RoosterOil.CMS
{
    public class CreativeCacheManager : BackgroundService
    {
        private readonly ILogger<CreativeCacheManager> _logger;

        public CreativeCacheManager(ILogger<CreativeCacheManager> logger)
        {
            _logger = logger;
        }

        private async Task RetrieveAccountCacheAsync(string accountId)
        {
            try
            {
                _logger.LogInformation($"Refreshing cache items for account {accountId}.");

                var fullPath = Path.Combine(DocStoreConfiguration.GetDocStoreConfiguration()._creativeCacheOptions.Path, accountId);

                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                var db = DbDocument.GetStore(_logger);

                var creatives = await DocumentDbQuery.GetItemsMatching<Creative>(db, nameof(Creative._accountId), accountId).ConfigureAwait(false);

                int itemsRefreshed = 0;

                foreach (var creative in creatives)
                {
                    if (!await creative.CacheUpToDate(fullPath))
                    {

                        var remoteCache = new RemoteSystem(DocStoreConfiguration.GetDocStoreConfiguration()._remoteSystemOptions);

                        var remoteFileName = creative._accountId + '/' + creative.CreativeFileName;
                        _logger.LogInformation($"Moving creative {remoteFileName} ({creative.name}) to local cache.");
                        remoteCache.CopyFrom(remoteFileName, fullPath);
                        itemsRefreshed++;
                    }
                }

                if (itemsRefreshed != 0)
                {
                    _logger.LogInformation($"Cache refresh completed for account {accountId}.  {itemsRefreshed} out of {creatives.Count()} items refreshed.");
                }
                else
                {
                    _logger.LogInformation($"Cache refresh completed for account {accountId}. All items up-to-date.");
                }
            }
            catch (Exception)
            {
                _logger.LogError($"Error retrieving cache items for account {accountId}.");
                throw;
            }
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _logger.LogInformation("Cache refresh thread started");

                do
                {
                    List<Task> accountRefreshTasks = new List<Task>();

                    var db = DbDocument.GetStore(_logger);
                    var accounts = await DocumentDbQuery.GetAllItems<Account>(db).ConfigureAwait(false);

                    foreach (var account in accounts)
                    {
                        accountRefreshTasks.Add(Task.Run(() => RetrieveAccountCacheAsync(account._id), stoppingToken));
                    }

                    try
                    {
                        Task.WaitAll(accountRefreshTasks.ToArray(), stoppingToken);
                    }
                    catch (AggregateException ag)
                    {
                        foreach (var ex in ag.InnerExceptions)
                        {
                            _logger.LogError(ex.Message);
                        }
                    }

                    await Task.Delay(DocStoreConfiguration.GetDocStoreConfiguration()._creativeCacheOptions.RefreshRate * 1000);

                } while (!stoppingToken.IsCancellationRequested);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
