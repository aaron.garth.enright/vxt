using Microsoft.AspNetCore.Authentication.JwtBearer;
using IdentityServer4.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RoosterOil.CMS.Areas.Identity;
using RoosterOil.CMS.Data;
using RoosterOil.CMS.Models;
using RoosterOil.Identity;
using RoosterOil.DocumentDb;
using RoosterOil.Utility;
using System.Reflection;
using System.Text;

namespace RoosterOil.CMS
{
    public class Startup
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            log.Info("Starting initialization");
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.Configure<DocumentDbOptions>(Configuration.GetSection(DocumentDbOptions.DocumentDb));
            services.Configure<RemoteSystemOptions>(Configuration.GetSection(RemoteSystemOptions.RemoteSystem));
            services.Configure<CreativeCacheOptions>(Configuration.GetSection(CreativeCacheOptions.CacheInformation));
            services.Configure<VideoEncoderOptions>(Configuration.GetSection(VideoEncoderOptions.VideoReEncoder));

            services.AddHostedService<CreativeCacheManager>();
            services.AddHostedService<InitializationService>();

            services.ConfigureNonBreakingSameSiteCookies();
            
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("UserConnection")));

            services.AddDefaultIdentity<ApplicationUser>(options =>
                    options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();
            
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            var builder = services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;

                options.UserInteraction = new UserInteractionOptions
                {
                    LogoutUrl = "/Account/Logout",
                    LoginUrl = "/Account/Login",
                    LoginReturnUrlParameter = "returnUrl"
                };
            })
            .AddAspNetIdentity<ApplicationUser>()
            // this adds the config data from DB (clients, resources, CORS)
            .AddConfigurationStore(options =>
            {
                options.ConfigureDbContext = db =>
                    db.UseMySql(Configuration.GetConnectionString("IS4Configuration"),
                        sql => sql.MigrationsAssembly(migrationsAssembly));
            })
            // this adds the operational data from DB (codes, tokens, consents)
            .AddOperationalStore(options =>
            {
                options.ConfigureDbContext = db =>
                    db.UseMySql(Configuration.GetConnectionString("IS4Configuration"),
                        sql => sql.MigrationsAssembly(migrationsAssembly));

                // this enables automatic token cleanup. this is optional.
                options.EnableTokenCleanup = true;
                // options.TokenCleanupInterval = 15; // interval in seconds. 15 seconds useful for debugging
            });

            // not recommended for production - you need to store your key material somewhere secure
            builder.AddDeveloperSigningCredential();

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();
            services.AddControllers();
            services.AddAuthentication()
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Tokens.GetSignature())),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                })
                .AddGoogle(options =>
                {
                    options.ClientId = "782056070717-a59t5hh3r8pvs03urmtu66ulqej2q4ek.apps.googleusercontent.com";
                    options.ClientSecret = "WOIAdRA1AP770cq-Jt_aPoGO";
                    options.CorrelationCookie.SameSite = SameSiteMode.Lax;
                })
                .AddTwitter(options =>
                {
                    options.ConsumerKey = "rP7XIjARtKBY7W7WjISvnknKe";
                    options.ConsumerSecret = "sJfRhbO2ZvYvJAVXymYTTGhvBxC6pRpbFnTn4ugKk2dWmElakW";
                    options.RetrieveUserDetails = true;
                })
/*                .AddFacebook()
                .AddMicrosoftAccount()*/;
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RoosterOil.API.Server", Version = "v1" });
            });
            log.Info("All Services Started");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RoosterOil.API.Server v1"));
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseIdentityServer();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
