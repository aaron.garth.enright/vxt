﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using MySqlX.XDevAPI;
using MySqlX.XDevAPI.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RoosterOil.DisplayAPI.Client.Data;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;

namespace RoosterOil.DisplayAPI.Controllers
{
    [ApiController]
    [Route("api/display")]
    public class DisplayAPI : Controller
    {
        private readonly ILogger<DisplayAPI> _logger;
        private ConcurrentDictionary<string, int> deviceWorkloadSequences;

        public DisplayAPI(ILogger<DisplayAPI> logger)
        {
            _logger = logger;
            deviceWorkloadSequences = new ConcurrentDictionary<string, int>();
        }

        [HttpPost("getid")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetDeviceId(Device device)
        {
            _logger.LogInformation($"A device with serial number {device.serialNumber} has requested an id.");
            var db = DbDocument.GetStore(_logger);

            var keys = await DocumentDbQuery.GetKeysMatching<Creative>(db,
                Query.AND(
                    Query.AND(
                        Query.EQ<Device>(nameof(device.serialNumber), device.serialNumber),
                        Query.EQ<Device>(nameof(device.ethernetMAC), device.ethernetMAC)),
                    Query.EQ<Device>(nameof(device.wirelessMAC), device.wirelessMAC))
                ).ConfigureAwait(false);

            if (keys.Count() == 0)
            {
                return NotFound();
            }

            var id = keys.FirstOrDefault()._id;

            _logger.LogInformation($"Found a matching device with id {id}.");

            return Ok(id);
        }

        [HttpGet("getschedule/{deviceId}/{scheduleId}/{lastUpdated}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<WorkloadResponse>> GetScheduleChanges(string deviceId, string scheduleId, DateTime lastUpdated)
        {
            _logger.LogInformation($"Schedule request for device {deviceId}");
            var db = DbDocument.GetStore(_logger);

            if (String.IsNullOrEmpty(deviceId))
            {
                return BadRequest("Device Id cannot be empty.");
            }

            var deviceKeys = (await DocumentDbQuery.GetKeysMatching<Device>(db, nameof(Device.deviceId), deviceId).ConfigureAwait(false));

            if (deviceKeys.Count() == 0)
            {
                return BadRequest($"Device Id {deviceId} not found.");
            }

            if (deviceKeys.Count() > 1)
            {
                _logger.LogWarning($"Device Id {deviceId} assigned to multiple devices.");
            }

            var dbDeviceId = deviceKeys.FirstOrDefault()._id;

            if (!deviceWorkloadSequences.ContainsKey(dbDeviceId))
            {
                if (deviceWorkloadSequences.TryAdd(dbDeviceId, 0))
                {
                    _logger.LogInformation($"Sequence added for {deviceId}");
                }
                else
                {
                    _logger.LogWarning($"Failed to add sequence for {deviceId}.  Sequences returned may be invalid.");
                }
            }

            if (lastUpdated == DateTime.MinValue.ToUniversalTime())
            {
                _logger.LogInformation("First call from device this session.");
            }
            else if (deviceWorkloadSequences.ContainsKey(dbDeviceId))
            {
                deviceWorkloadSequences[dbDeviceId]++;
            }

            var schedules = await DocumentDbQuery.GetItemsContaining<Schedule>(db, nameof(Schedule._deviceIds), dbDeviceId).ConfigureAwait(false);

            _logger.LogInformation($"Found {schedules.Count()} schedules for device {deviceId}");

            var nextSchedule = (from schedule in schedules
                                where schedule.endTime > DateTime.Now.ToUniversalTime()
                                orderby schedule.startTime descending
                                select schedule).FirstOrDefault();

            if (nextSchedule != null)
            {
                _logger.LogInformation($"Next schedule: {nextSchedule._id} starting at {nextSchedule.startTime}.");
                await nextSchedule.Load(db);
            }
            else
            {
                _logger.LogInformation($"All schedules expired.");
            }

            var response = new ScheduleChangesResponse()
            {
                sequence = deviceWorkloadSequences[dbDeviceId]
            };

            if (nextSchedule == null || nextSchedule._id != scheduleId || nextSchedule.lastModifiedTime > lastUpdated)
            {
                response.newSchedule = nextSchedule;
            }

            response.responseTime = DateTime.Now.ToUniversalTime();
            return Ok(response);
        }

        [HttpGet("download/{file}")]
        [ProducesResponseType(typeof(File), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public Task<ActionResult> Download(string file)
        {
            var fileName = @"wwwroot/cache/" + file;

            if (!System.IO.File.Exists(fileName))
            {
                return Task.FromResult<ActionResult>(NotFound());
            }

            return Task.FromResult<ActionResult>(File(System.IO.File.OpenRead(fileName), FileData.GetContentTypeFromMIMETable(fileName)));
        }

        [HttpGet("cacheitem/{creativeId}")]
        [ProducesResponseType(typeof(File), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Cacheitem(string creativeId)
        {
            _logger.LogInformation($"Request for creative {creativeId}");
            var db = DbDocument.GetStore(_logger);

            var creative = await DocumentDbQuery.Get<Creative>(db, creativeId).ConfigureAwait(false);

            var fileName = Path.Combine(DocStoreConfiguration.GetDocStoreConfiguration()._creativeCacheOptions.Path, creative._accountId, creative.CreativeFileName);

            if (!System.IO.File.Exists(fileName))
            {
                return NotFound();
            }

            return File(System.IO.File.OpenRead(fileName), FileData.GetContentTypeFromMIMETable(fileName));
        }

        [HttpGet("ads/{deviceId}")]
        [ProducesResponseType(typeof(Advertisement), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public Task<ActionResult<Advertisement>> GetAds(long deviceId)
        {
            if (deviceId != 6)
            {
                return Task.FromResult<ActionResult<Advertisement>>(NotFound());
            }

            return Task.FromResult<ActionResult<Advertisement>>(Ok(new Advertisement()
            {
                numberOfPlays = 10,
                locationData = "images/AdSpeedLogoIntro.mp4",
                fileEncoding = "video/mp4"
            }));
        }
    }
}
