﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RoosterOil.Shared;
using RoosterOil.ContentAPI.Client.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(Account.apiLocation)]
    public partial class AccountAPI : Controller
    {
        private readonly ILogger<AccountAPI> _logger;

        public AccountAPI(ILogger<AccountAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Account>> StoreAccount(Account account) => await new APIHelper(_logger).Store<Account>(account, Request.Path).ConfigureAwait(false);

        [HttpPost("create")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Account>> CreateAccount(AccountData accountData)
        {
            return await StoreAccount(new Account()
            {
                number = accountData.number,
                name = accountData.name,
                location = (Location)accountData
            }).ConfigureAwait(false);
        }

        [HttpGet("read/{accountId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin, SiteAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Account>> ReadAccount(string accountId) => await new APIHelper(_logger).Read<Account>(accountId).ConfigureAwait(false);

        [HttpGet("getby/{field}/{value}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin, SiteAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<Account>>> GetAccountBy(string field, string value) => await new APIHelper(_logger).GetBy<Account>(field, value).ConfigureAwait(false);

        [HttpPost("update")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateAccount(Account account) => await new APIHelper(_logger).Update<Account>(account).ConfigureAwait(false);

        [HttpDelete("delete/{accountId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAccount(string accountId) => await new APIHelper(_logger).Delete<Account>(accountId).ConfigureAwait(false);
    }
}
