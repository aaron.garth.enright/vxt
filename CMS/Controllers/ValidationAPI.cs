﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Identity;
using RoosterOil.Shared;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(Tokens.apiLocation)]
    public class ValidationAPI : Controller
    {
        private readonly ILogger<ValidationAPI> _logger;

        protected Tokens.TokenType ParseTokenType(string tokenTypeString)
        {
            try
            {
                return Enum.Parse<Tokens.TokenType>(tokenTypeString);
            }
            catch (ArgumentException)
            {
                return Tokens.TokenType.Invalid;
            }
        }

        public ValidationAPI(ILogger<ValidationAPI> logger)
        {
            _logger = logger;
        }

        [HttpGet("device")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Device")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<string>> RevalidateDevice()
        {
            var claimsIdentity = HttpContext.User.Claims as ClaimsIdentity;
            var deviceId = claimsIdentity.FindFirst(ClaimTypes.Name).Value;
            var venueId = claimsIdentity.FindFirst(ClaimTypes.Locality).Value;

            if (String.IsNullOrEmpty(deviceId) || String.IsNullOrEmpty(venueId))
            {
                return Unauthorized("Bad token.");
            }

            var db = DbDocument.GetStore(_logger);

            var devices = await DocumentDbQuery.GetItemsMatching<Device>(db, Query.AND(Query.EQ<Device>(nameof(Device.deviceId), deviceId), Query.EQ<Device>(nameof(Device._venueId), venueId))).ConfigureAwait(false);

            if (devices.Count() == 0)
            {
                return Unauthorized("No matching device found.");
            }

            if (devices.Count() > 1)
            {
                throw new Exception("Internal Server Error:  Multiple devices with the same deviceId");
            }

            var device = devices.FirstOrDefault();

            if (!device.isActive)
            {
                return Unauthorized("Device deactivated.");
            }

            return Ok(Tokens.CreateDeviceToken(device));
        }

        [HttpGet("user")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<string>> RevalidateUser()
        {
            var claimsIdentity = HttpContext.User.Claims as ClaimsIdentity;
            var accountId = claimsIdentity.FindFirst(ClaimTypes.Name).Value;
            var emailAddress = claimsIdentity.FindFirst(ClaimTypes.Email).Value;
            var tokenType = ParseTokenType(claimsIdentity.FindFirst(ClaimTypes.Role).Value);

            if (String.IsNullOrEmpty(accountId) || String.IsNullOrEmpty(emailAddress) || tokenType == Tokens.TokenType.Invalid)
            {
                return Unauthorized("Bad token.");
            }

            var db = DbDocument.GetStore(_logger);

            var users = await DocumentDbQuery.GetItemsMatching<EndUser>(db, Query.AND(Query.EQ<EndUser>(nameof(EndUser.emailAddress), emailAddress), Query.EQ<EndUser>(nameof(EndUser._accountId), accountId))).ConfigureAwait(false);

            if (users.Count() == 0)
            {
                return Unauthorized("User not found.");
            }

            if (users.Count() > 1)
            {
                throw new Exception("Internal Server Error:  Multiple users with the same email and account.");
            }

            var user = users.FirstOrDefault();

            if (!user.isActive)
            {
                return Unauthorized("User deactivated.");
            }

            var account = await DocumentDbQuery.Get<Account>(db, accountId).ConfigureAwait(false);

            if (account == null || !account.isActive)
            {
                return Unauthorized("Invalid account.");
            }

            return Ok(Tokens.CreateUserToken(account, user, tokenType));
        }

        [HttpGet("bearer/{accountId}/{userId}/{tokenType}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<string>> IssueBearerToken(string accountId, string userId, string tokenType)
        {
            Tokens.TokenType resultTokenType = ParseTokenType(tokenType);

            if (String.IsNullOrEmpty(accountId) || String.IsNullOrEmpty(userId) || resultTokenType == Tokens.TokenType.Invalid)
            {
                return BadRequest("Invalid Parameters");
            }

            var claimsIdentity = HttpContext.User.Claims as ClaimsIdentity;
            var requestAccountId = claimsIdentity.FindFirst(ClaimTypes.Name).Value;
            var emailAddress = claimsIdentity.FindFirst(ClaimTypes.Email).Value;
            var requestTokenType = ParseTokenType(claimsIdentity.FindFirst(ClaimTypes.Role).Value);

            if (String.IsNullOrEmpty(requestAccountId) || String.IsNullOrEmpty(emailAddress) || requestTokenType == Tokens.TokenType.Invalid)
            {
                return Unauthorized("Bad token.");
            }

            var db = DbDocument.GetStore(_logger);

            var users = await DocumentDbQuery.GetItemsMatching<EndUser>(db, Query.AND(Query.EQ<EndUser>(nameof(EndUser.emailAddress), emailAddress), Query.EQ<EndUser>(nameof(EndUser._accountId), accountId))).ConfigureAwait(false);

            if (users.Count() == 0)
            {
                return Unauthorized("Requesting user not found.");
            }

            if (users.Count() > 1)
            {
                throw new Exception("Internal Server Error:  Multiple users with the same email and account.");
            }

            var requestingUser = users.FirstOrDefault();

            if (!requestingUser.isActive)
            {
                return Unauthorized("Requesting user deactivated.");
            }

            var requestingAccount = await DocumentDbQuery.Get<Account>(db, requestAccountId).ConfigureAwait(false);

            if (requestingAccount == null || !requestingAccount.isActive)
            {
                return Unauthorized("Requesting account invalid or disabled.");
            }

            var account = await DocumentDbQuery.Get<Account>(db, accountId).ConfigureAwait(false);

            if (account == null || !account.isActive)
            {
                return BadRequest("Account invalid or disabled.");
            }

            var user = await DocumentDbQuery.Get<EndUser>(db, userId).ConfigureAwait(false);

            if (user == null || !user.isActive)
            {
                return BadRequest("Account invalid or disabled.");
            }

            return Ok(Tokens.CreateBearerToken(account, user, resultTokenType));
        }

    }
}
