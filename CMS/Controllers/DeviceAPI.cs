﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(Device.apiLocation)]
    public partial class DeviceAPI : Controller
    {
        private readonly ILogger<DeviceAPI> _logger;

        public DeviceAPI(ILogger<DeviceAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Device>> StoreDevice(Device device) => await new APIHelper(_logger).Store<Device>(device, Request.Path).ConfigureAwait(false);

        [HttpGet("read/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Device>> ReadDevice(string deviceId) => await new APIHelper(_logger).Read<Device>(deviceId).ConfigureAwait(false);

        [HttpGet("getby/{field}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<Device>>> GetDeviceBy(string field, string value) => await new APIHelper(_logger).GetBy<Device>(field, value).ConfigureAwait(false);

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateDevice(Device device) => await new APIHelper(_logger).Update<Device>(device).ConfigureAwait(false);

        [HttpDelete("delete/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteDevice(string deviceId) => await new APIHelper(_logger).Delete<Device>(deviceId).ConfigureAwait(false);
    }
}
