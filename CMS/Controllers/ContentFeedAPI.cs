﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(ContentFeed.apiLocation)]
    public partial class ContentFeedAPI : Controller
    {
        private readonly ILogger<ContentFeedAPI> _logger;

        public ContentFeedAPI(ILogger<ContentFeedAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<ContentFeed>> StoreContentFeed(ContentFeed contentFeed) => await new APIHelper(_logger).Store<ContentFeed>(contentFeed, Request.Path).ConfigureAwait(false);

        [HttpPost("create/{accountId}/{name}/{type}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<ContentFeed>> CreateContentFeed(string accountId, string contentFeedName, ContentType contentType)
        {
            return await StoreContentFeed(new ContentFeed()
            {
                _accountId = accountId,
                name = contentFeedName,
                type = contentType
            }).ConfigureAwait(false);
        }

        [HttpPost("add/{contentFeedId}/{contentFeedItem}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> AddContentFeedItem(string contentFeedId, ContentFeedItem contentFeedItem)
        {
            var db = DbDocument.GetStore(_logger);

            var contentFeeds = await DocumentDbQuery.GetItemsMatching<ContentFeed>(db, nameof(ContentFeed._id), contentFeedId).ConfigureAwait(false);

            if (contentFeeds.Count() > 1)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var contentFeed = contentFeeds.FirstOrDefault();

            contentFeed.contentFeedItems.Add(contentFeedItem);

            return await UpdateContentFeed(contentFeed);
        }

        [HttpGet("read/{contentFeedId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ContentFeed>> ReadContentFeed(string contentFeedId) => await new APIHelper(_logger).Read<ContentFeed>(contentFeedId).ConfigureAwait(false);

        [HttpGet("getby/{field}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<ContentFeed>>> GetContentFeedBy(string field, string value) => await new APIHelper(_logger).GetBy<ContentFeed>(field, value).ConfigureAwait(false);

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateContentFeed(ContentFeed ContentFeed) => await new APIHelper(_logger).Update<ContentFeed>(ContentFeed).ConfigureAwait(false);

        [HttpDelete("delete/{contentFeedId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteContentFeed(string contentFeedId) => await new APIHelper(_logger).Delete<ContentFeed>(contentFeedId).ConfigureAwait(false);
    }
}
