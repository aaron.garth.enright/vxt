﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    public class APIHelper : Controller
    {
        private readonly ILogger<Controller> _logger;

        public APIHelper(ILogger<Controller> logger)
        {
            _logger = logger;
        }

        public async Task<ActionResult<T>> Store<T> (T item, string requestPath) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            var db = DbDocument.GetStore(_logger);

            string newId = "";
            try
            {
                var result = await DbDocument.Create<T>(db, item).ConfigureAwait(false);
                newId = result.GeneratedIds.FirstOrDefault();
            }
            catch (DocumentDbException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }

            item._id = newId;
            _logger.LogInformation($"{nameof(T)} created with id {newId}");

            return Created(requestPath, item);
        }

        public async Task<ActionResult<T>> Read<T>(string id) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            var db = DbDocument.GetStore(_logger);
            var item = await DocumentDbQuery.Get<T>(db, id).ConfigureAwait(false);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(await item.Normalize().ConfigureAwait(false));
        }

        public async Task<ActionResult<IEnumerable<T>>> GetBy<T>(string field, string value) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            var db = DbDocument.GetStore(_logger);
            
            try
            {
                var items = (await DocumentDbQuery.GetItemsMatching<T>(db, field, value).ConfigureAwait(false));

                if (items.Count() == 0)
                {
                    return NotFound();
                }

                var returnList = new List<T>();
                foreach (var item in items)
                {
                    returnList.Add(await item.Normalize().ConfigureAwait(false));
                }

                return Ok(returnList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<IActionResult> Update<T>(T item) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            var db = DbDocument.GetStore(_logger);

            if (!await DocumentDbQuery.Exists<T>(db, nameof(DbDocument._id), item._id).ConfigureAwait(false))
            {
                return NotFound();
            }

            try
            {
                var result = await DbDocument.Update<T>(db, item).ConfigureAwait(false);
            }
            catch (DocumentDbException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        public async Task<IActionResult> Delete<T>(string id) where T : DbDocument, INormalizedDbDocument<T>, new()
        {
            var db = DbDocument.GetStore(_logger);

            var item = (await DocumentDbQuery.Get<T>(db, id).ConfigureAwait(false));

            if (item == null)
            {
                return NotFound();
            }

            try
            {
                var result = await DbDocument.Delete<T>(db, item).ConfigureAwait(false);
            }
            catch (DocumentDbException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }

            return Ok();
        }
    }
}
