﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(Creative.apiLocation)]
    public partial class CreativeAPI : Controller
    {
        private readonly ILogger<CreativeAPI> _logger;

        public CreativeAPI(ILogger<CreativeAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Creative>> StoreCreative(Creative Creative) =>
            await new APIHelper(_logger).Store<Creative>(Creative, Request.Path).ConfigureAwait(false);

        [HttpPost("upload")]
        [RequestSizeLimit(int.MaxValue * 32L)]
        [RequestFormLimits(MultipartBodyLengthLimit = int.MaxValue *32L)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<IEnumerable<Creative>>> CreateCreative([FromQuery] string accountId, [FromQuery] bool overwriteExising)
        {
            bool overwriteExisting = false;

            var returnList = new List<Creative>();

            if (String.IsNullOrEmpty(accountId))
            {
                return BadRequest("Required parameter accountId not passed.");
            }

            var db = DbDocument.GetStore(_logger);

            if (!await DocumentDbQuery.Exists<Account>(db, nameof(Account._id), accountId))
            {
                return BadRequest("Invalid accountId.");
            }

            if (Request?.Form?.Files == null)
            {
                return BadRequest("No files attached to create request.");

            }

            foreach (var file in Request.Form.Files)
            {
                var fileName = file.FileName;
                if (file.Length <= 0)
                {
                    return BadRequest($"Zero length file {fileName} detected.");
                }

                if (!overwriteExisting)
                {
                    var uniqueCreativeQuery = Query.AND(Query.EQ<Account>(nameof(Account._id), accountId), Query.EQ<Creative>(nameof(Creative.name), fileName));

                    var existingCreative = await DocumentDbQuery.GetKeysMatching<Creative>(db, uniqueCreativeQuery).ConfigureAwait(false);

                    if (existingCreative.Count > 0)
                    {
                        return BadRequest($"Creative {fileName} already exists in account {accountId} and overwriteExisting is set to false.");
                    }
                }
            }

            foreach (var file in Request.Form.Files)
            {
                var fileName = file.FileName;
                var filePath = Path.ChangeExtension(Path.GetTempFileName(), Path.GetExtension(fileName));

                using (var stream = System.IO.File.Create(filePath))
                {
                    await file.CopyToAsync(stream);
                    stream.Close();
                }
                var creative = await Creative.CreateFromFile(filePath).ConfigureAwait(false);
                creative.name = file.FileName;
                creative._accountId = accountId;

                if (overwriteExising)
                {
                    var uniqueCreativeQuery = Query.AND(Query.EQ<Account>(nameof(Account._id), accountId), Query.EQ<Creative>(nameof(Creative.name), fileName));

                    var existingKeys = await DocumentDbQuery.GetKeysMatching<Creative>(db, uniqueCreativeQuery).ConfigureAwait(false);

                    if (existingKeys.Count > 1)
                    {
                        throw new Exception("Internal Server Error:  Multiple creatives with same name in same account.");
                    }
                    else if (existingKeys.Count == 1)
                    {
                        creative._id = existingKeys.FirstOrDefault()._id;
                    }
                }

                if (creative.IsNew())
                {
                    var result = await DbDocument.Create<Creative>(db, creative).ConfigureAwait(false);
                    creative._id = result.GeneratedIds.FirstOrDefault();
                }
                else
                {
                    await DbDocument.Update<Creative>(db, creative).ConfigureAwait(false);
                }

                returnList.Add(creative);
            }

            return Created(Request.Path, returnList);
        }

        [HttpGet("read/{creativeId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Creative>> ReadCreative(string creativeId) => await new APIHelper(_logger).Read<Creative>(creativeId).ConfigureAwait(false);

        [HttpGet("getinfoby/{field}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<Creative>>> GetCreativeBy(string field, string value) => await new APIHelper(_logger).GetBy<Creative>(field, value).ConfigureAwait(false);

        [HttpGet("download/{creativeId}")]
        [ProducesResponseType(typeof(File), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Cacheitem(string creativeId)
        {
            // TODO: when bearer tokens are working, ensure that caller has rights to this creative.
            _logger.LogInformation($"Request for creative {creativeId}");
            var db = DbDocument.GetStore(_logger);

            var creative = await DocumentDbQuery.Get<Creative>(db, creativeId).ConfigureAwait(false);

            var fileName = Path.Combine(DocStoreConfiguration.GetDocStoreConfiguration()._creativeCacheOptions.Path, creative._accountId, creative.CreativeFileName);

            if (!System.IO.File.Exists(fileName))
            {
                return NotFound();
            }

            return File(System.IO.File.OpenRead(fileName), FileData.GetContentTypeFromMIMETable(fileName));
        }

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateCreative(Creative Creative) => await new APIHelper(_logger).Update<Creative>(Creative).ConfigureAwait(false);

        [HttpDelete("delete/{creativeId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCreative(string creativeId) => await new APIHelper(_logger).Delete<Creative>(creativeId).ConfigureAwait(false);
    }
}
