﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(EndUser.apiLocation)]
    public partial class UserAPI : Controller
    {
        private readonly ILogger<UserAPI> _logger;

        public UserAPI(ILogger<UserAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<EndUser>> StoreUser(EndUser user) => await new APIHelper(_logger).Store<EndUser>(user, Request.Path).ConfigureAwait(false);

        [HttpPost("create/{accountId}/{firstName}/{lastName}/{emailAddress}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<EndUser>> CreateUser(string accountId, string first, string last, string email)
        {
            return await StoreUser(new EndUser()
            {
                _accountId = accountId,
                firstName = first,
                lastName = last,
                emailAddress = email
            }).ConfigureAwait(false);
        }

        [HttpGet("read/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EndUser>> ReadUser(string userId) => await new APIHelper(_logger).Read<EndUser>(userId).ConfigureAwait(false);

        [HttpGet("getby/{field}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<EndUser>>> GetUserBy(string field, string value) => await new APIHelper(_logger).GetBy<EndUser>(field, value).ConfigureAwait(false);

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateUser(EndUser user) => await new APIHelper(_logger).Update<EndUser>(user).ConfigureAwait(false);

        [HttpDelete("delete/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteUser(string userId) => await new APIHelper(_logger).Delete<EndUser>(userId).ConfigureAwait(false);
    }
}
