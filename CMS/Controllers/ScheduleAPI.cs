﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(Schedule.apiLocation)]
    public partial class ScheduleAPI : Controller
    {
        private readonly ILogger<ScheduleAPI> _logger;

        public ScheduleAPI(ILogger<ScheduleAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Schedule>> StoreSchedule(Schedule schedule) => await new APIHelper(_logger).Store<Schedule>(schedule, Request.Path).ConfigureAwait(false);

        [HttpPost("create/{accountId}/{contentFeedLoopId}/{startTime}/{endTime}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Schedule>> CreateSchedule(string accountId, string contentFeedLoopId, DateTime startTime, DateTime endTime)
        {
            return await StoreSchedule(new Schedule()
            {
                _accountId = accountId,
                _contentFeedLoopId = contentFeedLoopId,
                startTime = startTime,
                endTime = endTime
            }).ConfigureAwait(false);
        }

        [HttpPost("add/{scheduleId}/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> AddDevice(string scheduleId, string deviceId)
        {
            var db = DbDocument.GetStore(_logger);

            var schedules = await DocumentDbQuery.GetItemsMatching<Schedule>(db, nameof(Schedule._id), scheduleId).ConfigureAwait(false);

            if (schedules.Count() > 1)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (!await DocumentDbQuery.Exists<Device>(db, nameof(Device._id), scheduleId).ConfigureAwait(false) || schedules.Count() == 0)
            {
                return NotFound();
            }

            var schedule = schedules.FirstOrDefault();

            schedule._deviceIds.Add(deviceId);

            return await UpdateSchedule(schedule);
        }

        [HttpGet("read/{scheduleId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Schedule>> ReadSchedule(string scheduleId) => await new APIHelper(_logger).Read<Schedule>(scheduleId).ConfigureAwait(false);

        [HttpGet("getby/{field}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<Schedule>>> GetScheduleBy(string field, string value) => await new APIHelper(_logger).GetBy<Schedule>(field, value).ConfigureAwait(false);

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateSchedule(Schedule schedule) => await new APIHelper(_logger).Update<Schedule>(schedule).ConfigureAwait(false);

        [HttpDelete("delete/{scheduleId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteSchedule(string scheduleId) => await new APIHelper(_logger).Delete<Schedule>(scheduleId).ConfigureAwait(false);
    }
}
