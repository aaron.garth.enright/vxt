﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(ContentFeedLoop.apiLocation)]
    public partial class ContentFeedLoopAPI : Controller
    {
        private readonly ILogger<ContentFeedLoopAPI> _logger;

        public ContentFeedLoopAPI(ILogger<ContentFeedLoopAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("save")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<ContentFeedLoop>> StoreContentFeedLoop(ContentFeedLoop contentFeedLoop) => await new APIHelper(_logger).Store<ContentFeedLoop>(contentFeedLoop, Request.Path).ConfigureAwait(false);

        [HttpPost("create/{accountId}/{contentFeedLoopName}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<ContentFeedLoop>> CreateContentFeedLoop(string accountId, string contentFeedLoopName)
        {
            return await StoreContentFeedLoop(new ContentFeedLoop()
            {
                _accountId = accountId,
                name = contentFeedLoopName
            }).ConfigureAwait(false);
        }

        [HttpGet("read/{contentFeedLoopId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ContentFeedLoop>> ReadContentFeedLoop(string contentFeedLoopId) => await new APIHelper(_logger).Read<ContentFeedLoop>(contentFeedLoopId).ConfigureAwait(false);

        [HttpGet("getby/{field}/{value}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<ContentFeedLoop>>> GetContentFeedLoopBy(string field, string value) => await new APIHelper(_logger).GetBy<ContentFeedLoop>(field, value).ConfigureAwait(false);

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateContentFeedLoop(ContentFeedLoop contentFeedLoop) => await new APIHelper(_logger).Update<ContentFeedLoop>(contentFeedLoop).ConfigureAwait(false);

        [HttpDelete("delete/{contentFeedLoopId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteContentFeedLoop(string contentFeedLoopId) => await new APIHelper(_logger).Delete<ContentFeedLoop>(contentFeedLoopId).ConfigureAwait(false);
    }
}
