﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RoosterOil.CMS.Controllers
{
    [ApiController]
    [Route(Advertisement.apiLocation)]
    public partial class AdvertisementAPI : Controller
    {
        private readonly ILogger<AdvertisementAPI> _logger;

        public AdvertisementAPI(ILogger<AdvertisementAPI> logger)
        {
            _logger = logger;
        }

        [HttpPost("create")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(typeof(Schedule), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Advertisement>> CreateAdvertisement()
        {
            var slotDuration = Request.Query["slotDuration"];
            var numberOfPlays = Request.Query["numberOfPlays"];

            if (Request.Form.Files.Count() != 1)
            {
                return BadRequest("Only one advertisement may be created at a time.");
            }

            foreach (var file in Request.Form.Files)
            {
                if (file.Length > 0)
                {
                    var filePath = Path.GetTempFileName();

                    using (var stream = System.IO.File.Create(@"C:\Temp\" + Path.GetFileName(file.FileName)))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
            }

            return Created(Request.Path, new Advertisement()
            {
                numberOfPlays = String.IsNullOrEmpty(numberOfPlays) ? int.MaxValue : int.Parse(numberOfPlays)
            });
        }

        [HttpDelete("delete/{advertisementId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "FullAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAdvertisement(string advertisementId) => await new APIHelper(_logger).Delete<Advertisement>(advertisementId).ConfigureAwait(false);
    }
}
