﻿using Microsoft.IdentityModel.Tokens;
using RoosterOil.Shared;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RoosterOil.Identity
{
    public static class Tokens
    {
        public const string apiLocation = "api/validation";
        public enum TokenType
        {
            Invalid,
            FullAdmin,
            SiteAdmin,
            ApiUser,
            Device,
            ReadOnly
        };

        public static string GetSignature()
        {
            return "0xAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE";
        }

        private const int BEARER_EXPIRE_YEARS = 5; // 5 yr token for bearer
        private const double DEVICE_EXPIRE_DAYS = 30.0; // 30 day token for devices
        private const double USER_EXPIRE_HOURS = 2.0; // 2 hr token for interactive

        public static string CreateBearerToken(Account account, EndUser user, TokenType type)
        {
            var key = Encoding.ASCII.GetBytes(GetSignature());
            var tokenHandler = new JwtSecurityTokenHandler();
            var descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, account._id),
                    new Claim(ClaimTypes.Email, user.emailAddress),
                    new Claim(ClaimTypes.Role, type.ToString())
                }),
                Expires = DateTime.UtcNow.AddYears(BEARER_EXPIRE_YEARS),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(descriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string CreateDeviceToken(Device device)
        {
            var key = Encoding.ASCII.GetBytes(GetSignature());
            var tokenHandler = new JwtSecurityTokenHandler();
            var descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, device.deviceId),
                    new Claim(ClaimTypes.SerialNumber, device.serialNumber),
                    new Claim(ClaimTypes.Role, TokenType.Device.ToString()),
                    new Claim(ClaimTypes.Locality, device._venueId),
                }),
                Expires = DateTime.UtcNow.AddDays(DEVICE_EXPIRE_DAYS),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(descriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string CreateUserToken(Account account, EndUser user, TokenType type)
        {
            var key = Encoding.ASCII.GetBytes(GetSignature());
            var tokenHandler = new JwtSecurityTokenHandler();
            var descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, account._id),
                    new Claim(ClaimTypes.Email, user.emailAddress),
                    new Claim(ClaimTypes.Role, type.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(USER_EXPIRE_HOURS),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(descriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
