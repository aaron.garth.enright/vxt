﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using RestSharp;
using RoosterOil.DocumentDb;
using RoosterOil.Shared;
using RoosterOil.Utility;
using RoosterOil.ContentAPI.Client.Data;

namespace RoosterOil.ContentAPI.Client
{
    public class DataClient
    {
        private string _apiURL;

        private DataClient()
        {
        }

        public DataClient(string apiURL)
        {
            _apiURL = apiURL;
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        string bearerToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjAwMDA1ZmRmZWRmYzAwMDAwMDAwMDAwMDA4MDciLCJlbWFpbCI6InZ4dGRldmVsb3BlckBnbWFpbC5jb20iLCJyb2xlIjoiRnVsbEFkbWluIiwibmJmIjoxNjEyNTY4OTg4LCJleHAiOjE3NzAzMzUzODgsImlhdCI6MTYxMjU2ODk4OH0.Oc4jH1oO1Ce9RtjPPdVwFlVlyvx9wXFD0LbM-ITwHrc";

        private string GetAPILocation<T>() where T : IAPIAccessible
        {
            return typeof(T).GetField("apiLocation").GetValue("apiLocation").ToString();
        }

        public async Task<T> Save<T>(T item) where T : DbDocument, INormalizedDbDocument<T>, IAPIAccessible, new()
        {
            var client = new RestClient(_apiURL);
            client.AddDefaultHeader("Authorization", $"Bearer {bearerToken}");
            var request = new RestRequest($"{GetAPILocation<T>()}/save", Method.POST);

            request.AddJsonBody(await item.Normalize());

            var response = await client.ExecuteAsync<T>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.Created)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }

            return response.Data;
        }

        public async Task<T> Get<T>(string id) where T : DbDocument, INormalizedDbDocument<T>, IAPIAccessible, new()
        {
            var client = new RestClient(_apiURL);
            client.AddDefaultHeader("Authorization", $"Bearer {bearerToken}");
            var request = new RestRequest($"{GetAPILocation<T>()}/read/{id}", Method.GET);

            var response = await client.ExecuteAsync<T>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }

            return response.Data;
        }

        public async Task<IEnumerable<T>> GetBy<T>(string field, string value) where T : DbDocument, INormalizedDbDocument<T>, IAPIAccessible, new()
        {
            var client = new RestClient(_apiURL);
            client.AddDefaultHeader("Authorization", $"Bearer {bearerToken}");
            var request = new RestRequest($"{GetAPILocation<T>()}/getby/{field}/{value}", Method.GET);

            var response = await client.ExecuteAsync<IEnumerable<T>>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }

            return response.Data;
        }

        public async Task Update<T>(T item) where T : DbDocument, INormalizedDbDocument<T>, IAPIAccessible, new()
        {
            var client = new RestClient(_apiURL);
            client.AddDefaultHeader("Authorization", $"Bearer {bearerToken}");
            var request = new RestRequest($"{GetAPILocation<T>()}/update", Method.POST);

            request.AddJsonBody(item);

            var response = await client.ExecuteAsync<T>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                throw new Exception("Attempt to modify unknown account.");
            }

            if (response.StatusCode != HttpStatusCode.NoContent)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }
        }

        public async Task Delete<T>(string id) where T : DbDocument, INormalizedDbDocument<T>, IAPIAccessible, new()
        {
            var client = new RestClient(_apiURL);
            client.AddDefaultHeader("Authorization", $"Bearer {bearerToken}");
            var request = new RestRequest($"{GetAPILocation<T>()}/delete/{id}", Method.DELETE);

            var response = await client.ExecuteAsync<T>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }
        }

        public async Task<Account> Create(AccountData accountData)
        {
            var client = new RestClient(_apiURL);
            client.AddDefaultHeader("Authorization", $"Bearer {bearerToken}");
            var request = new RestRequest($"{Account.apiLocation}/create", Method.POST);
            request.AddJsonBody(accountData);

            var response = await client.ExecuteAsync<Account>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.Created)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }

            return response.Data;
        }

        public async Task<Account> GetAccountByName(string accountName)
        {
            return (await GetBy<Account>(nameof(Account.name), accountName).ConfigureAwait(false)).FirstOrDefault();
        }

        public async Task<Account> GetAccountByNumber(string accountNumber)
        {
            return (await GetBy<Account>(nameof(Account.number), accountNumber).ConfigureAwait(false)).FirstOrDefault();
        }

        public async Task<Venue> Create(VenueData venueData)
        {
            var client = new RestClient(_apiURL);
            var request = new RestRequest($"{Venue.apiLocation}/create", Method.POST);
            request.AddJsonBody(venueData);

            var response = await client.ExecuteAsync<Venue>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.Created)
            {
                throw new Exception($"Unknown Status Code Returned {response.StatusCode}");
            }

            return response.Data;
        }

        public async Task<Advertisement> CreateAdvertisement(string fileName, int duration, int numberOfPlays)
        {
            var client = new RestClient(_apiURL);
            var request = new RestRequest($"{Advertisement.apiLocation}/create", Method.POST);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddParameter("slotDuration", duration, ParameterType.QueryString);
            request.AddParameter("numberOfPlays", numberOfPlays, ParameterType.QueryString);

            var file = Path.GetFileName(fileName);
            var path = Path.GetDirectoryName(fileName);
            var content = FileData.GetContentTypeFromMIMETable(fileName);

            request.AddFile(file, fileName, content);
            var response = await client.ExecuteAsync<Advertisement>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            return response.Data;
        }

        public async Task<IEnumerable<Creative>> UploadCreatives(string accountId, List<String> fileNames, bool overwriteExisting = false)
        {
            var client = new RestClient(_apiURL);
            var request = new RestRequest($"{Creative.apiLocation}/upload", Method.POST);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddParameter("accountId", accountId, ParameterType.QueryString);
            request.AddParameter("overwriteExisting", overwriteExisting, ParameterType.QueryString);

            foreach (var fileName in fileNames)
            {
                var file = Path.GetFileName(fileName);
                var path = Path.GetDirectoryName(fileName);
                var content = FileData.GetContentTypeFromMIMETable(fileName);

                request.AddFile(file, fileName, content);
            }
            var response = await client.ExecuteAsync<IEnumerable<Creative>>(request).ConfigureAwait(false);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new Exception(response.Content);
            }

            return response.Data;
        }

    }
}
