﻿using RoosterOil.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoosterOil.ContentAPI.Client.Data
{
    public class VenueData : Location
    {
        public string name { get; set; }
        public string description { get; set; }
    }
}
