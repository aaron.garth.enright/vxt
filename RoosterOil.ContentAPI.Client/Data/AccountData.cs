﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoosterOil.Shared;

namespace RoosterOil.ContentAPI.Client.Data
{
    public class AccountData : Location
    {
        public string name { get; set; }
        public string number { get; set; }
    }
}
